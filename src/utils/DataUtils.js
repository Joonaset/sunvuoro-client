export const getTaskData = (id, tasks) => {
  const _tasks = [...tasks];
  let _task = {};

  for (const task of _tasks) {
    if (task.task_id === id) {
      _task = task;
      break;
    }
  }
  return _task;
};