export const hanldeFontSizeChange = option => {
  const { localStorage } = window;
  const html = document.querySelector(':root');
  if (option === 'large') {
    const fontSize = '32px';
    html.style.fontSize = fontSize;
    localStorage.setItem('fontSize', JSON.stringify(fontSize));
  } else if (option === 'medium') {
    const fontSize = '24px';
    html.style.fontSize = fontSize;
    localStorage.setItem('fontSize', JSON.stringify(fontSize));
  } else {
    const fontSize = '16px';
    html.style.fontSize = fontSize;
    localStorage.setItem('fontSize', JSON.stringify(fontSize));
  }
};
