import React from 'react';
import { getToken } from './AuthUtils';
import Navigation from '../components/Navigation/Navigation'

export const changeDivDisplayStyle = (div,show) => {
  if (document.getElementById(div) != null) {
    if (show) {
      document.getElementById(div).style.display = 'flex';
    } else {
      document.getElementById(div).style.display = 'none';
    }
  }
};

export const getNavigationBar = () => {
  if (getToken() != null) {
    return <Navigation style={{ display: 'flex' }} />;
  }
  return <Navigation style={{ display: 'none' }} />;
};
