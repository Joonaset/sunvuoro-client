export const getUser = () => {
  const userStr = sessionStorage.getItem('user');
  if (userStr) {
    try {
      const json = JSON.parse(userStr);
      return json;
    } catch (e) {
      return null;
    }
  }
  return {user: '', isAdmin: false};
};

export const getToken = () => {
  return sessionStorage.getItem('token') || null;
};

export const removeUserSession = () => {
  sessionStorage.removeItem('token');
  sessionStorage.removeItem('user');
};

export const setUserSession = (token, user) => {
  sessionStorage.setItem('token', token);
  sessionStorage.setItem('user', JSON.stringify(user));
};

export const checkRequiredCredentials = (user) => {
  if(getUser() !== null) {
    return user === 'maintainer'
    ? getUser().isAdmin
    : getToken();
  }

  return false;
};
