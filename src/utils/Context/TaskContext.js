import React from 'react';

const taskContext = React.createContext({
    name: '',
    description: '',
    header: '',
    handleValueChange: () => {},
    handleChange: () => {},
    location: '',
    fileSelectedHandler: () => {},
    buttonPressedHandler: () => {},
    data: {},
    loading: false,
    values: {ajattelu: 0, fysiikka: 0, sosiaalisuus: 0}
});

export default taskContext;