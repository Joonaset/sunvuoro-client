const url = 'http://localhost:4000';
const taskRoute = `${url}/api/tasks/`;
const loginRoute = `${url}/auth/login`;
const registerUserRoute = `${url}/auth/registerUser`
const checkUserExistsRoute = `${url}/auth/checkUser`
const locationRoute = `${url}/api/locations/`;
const citiesRoute = `${url}/api/cities/`;
const citiesWithTasksRoute = `${url}/api/cities/withTasks`;
const organizationRoute = `${url}/api/organizations/`
const dataCollectionRoute = `${url}/api/data/`
const applicationRoute = `${url}/api/application/`
const workTimesRoute = `${url}/api/worktimes/`


export const getTaskRoute = () => taskRoute;

export const getLoginRoute = () => loginRoute;

export const getRegisterUserRoute = () => registerUserRoute;

export const getLocationsRoute = () => locationRoute;

export const getCitiesRoute = () => citiesRoute;

export const getCitiesWithTasksRoute = () => citiesWithTasksRoute;

export const getOrganizationRoute = () => organizationRoute;

export const getDataCollectionRoute = () => dataCollectionRoute;

export const getApplicationRoute = () => applicationRoute;

export const getWorkTimesRoute = () => workTimesRoute;

export const getUserExistsRoute = () => checkUserExistsRoute;