import React from 'react';
import './Saavutettavuusseloste.css';

const Saavutettavuusseloste = () => {
  return (
    <div className="saavutettavuus-container">
      <h1 className="main-title">Saavutettavuusseloste</h1>
      <p>
        Tämä saavutettavuusseloste koskee verkkopalvelua Sun vuoro. Palvelua koskee laki digitaalisten palvelujen tarjoamisesta, jossa edellytetään
        että, julkisrahoitteisten verkkopalvelujen on oltava saavutettavia. Sivuston on täytettävä Web Content Accessibility Guidelines (WCAG) 2.1 kriteeristön
        A ja AA -tasot. Tämä saavutettavuusseloste on julkaistu muun sivuston julkaisun yhteydessä.
      </p>

      <h2>Saavutettavuusselosteen laatiminen</h2>
      <p>
        Tämä seloste on laadittu 10.5.2020. Seloste perustuu itsearvioon sekä automaattisilla testausohjelmilla suoritettuun testaamiseen.
      </p>

      <h2>Saavutettavuuden tila</h2>
      <p>Sun Vuoro -palvelu täyttää pääosin kriittiset saavutettavuusvaatimukset seuraavin havaituin puuttein.</p>

      <h2>Ei-saavutettava sisältö</h2>
      <h3>Kontekstin muutos syötteen perusteella</h3>
      <p>Löydä tehtäväsi -sivulla arvojen muuttaminen muuttaa myös suoraan tulosten lajitteluperustetta.</p>

      <h2>Palaute</h2>
      <p>
        Huomasitko saavutettavuuspuutteen digipalvelussamme? Kerro se meille ja teemme parhaamme puutteen korjaamiseksi. Yhteydenotto onnistuu sähköpostilla
        {/*ja palautelomakkeella*/}
        .
      </p>
      <p>
        {/*<LinkContainer to="/feedback" exact><button className="feedback-link" type="button">Palautelomake</button></LinkContainer>*/}
      </p>
      <p>
        <strong>Sähköposti: </strong>
        <a href="mailto:jari.pihlava@metropolia.fi">jari.pihlava@metropolia.fi</a>
      </p>

      <h2>Valvontaviranomainen</h2>
      <p>
        {`Jos huomaat sivustolla saavutettavuusongelmia, anna ensin palautetta meille eli sivuston ylläpitäjälle. 
          Vastauksessa voi mennä 14 päivää. Jos et ole tyytyväinen saamaasi vastaukseen tai et saa vastausta lainkaan kahden viikon aikana, `}
        <a href="https://www.saavutettavuusvaatimukset.fi/oikeutesi/" target="_blank" rel="noopener noreferrer">voit tehdä ilmoituksen Etelä-Suomen aluehallintovirastoon</a>
        {` Etelä-Suomen aluehallintoviraston sivulla kerrotaan tarkasti, 
          miten ilmoituksen voi tehdä ja miten asia käsitellään.`}
      </p>

      <h3>Valvontaviranomaisen yhteystiedot</h3>
      <p>
        Etelä-Suomen aluehallintovirasto
        <br />
        Saavutettavuuden valvonnan yksikkö
        <br />
        www.saavutettavuusvaatimukset.fi
        <br />
        <a href="mailto:saavutettavuus@avi.fi">saavutettavuus(at)avi.fi</a>
        <br />
        puhelinnumero vaihde <a href="tel:0295 016 000">0295 016 000</a>
      </p>
    </div>
  );
};

export default Saavutettavuusseloste;
