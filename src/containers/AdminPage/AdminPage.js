import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import { Tabs, Tab, Form, Accordion, Row, Card, Button } from 'react-bootstrap';
import { Multiselect } from 'multiselect-react-dropdown';
import Task from '../../components/TaskCard/Task';
import AdminModal from './AdminModal';
import { getTaskRoute, getLocationsRoute, getOrganizationRoute, getWorkTimesRoute } from '../../utils/Api';
import { getUser, getToken } from '../../utils/AuthUtils';
import { getTaskData } from '../../utils/DataUtils';
import DescriptionFields from '../../components/Forms/Tasks/TaskDescriptionFields';
import FileUploader from '../../components/Forms/Tasks/TaskFileUploader';
import ValueSelectors from '../../components/Forms/Tasks/TaskValueSelectors';
import Submitter from '../../components/Forms/Tasks/TaskSubmitter';
import TaskContext from '../../utils/Context/TaskContext';
import AdminInstructions from '../../components/Forms/Tasks/AdminInstructions';
import Popup from '../../components/Popup/Popup';
import Loader from '../../components/Loader';
import classes from './AdminPage.module.css';

const AdminPage = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const [tasks, setTasks] = useState([{
    task_id: 0,
    name: '',
    description: '',
    image: '',
    location_id: 0,
    ajattelu_value: 0,
    sosiaalisuus_value: 0,
    fysiikka_value: 0
  }]);
  const [modalData, setModalData] = useState({
    name: '',
    description: '',
    image: '',
    type: '',
    values: {
      sosiaalisuus: 1,
      fysiikka: 1,
      ajattelu: 1
    }
  });
  const [organization, setOrganization] = useState([]);
  const [taskFormData, setTaskFormData] = useState({
    selectedOrg: '',
    taskName: '',
    taskDescription: '',
    sosiaalisuus: 1,
    fysiikka: 1,
    ajattelu: 1,
    selectedFile: true
  });
  const [workTimes, setWorkTimes] = useState([{
    worktime_id: 0,
    worktime: '',
    selected: false
  }]);
  const [taskOpenedId, setTaskOpenedId] = useState(0);
  const [loading, setLoading] = useState(false);
  const [addExpanded, setAddExpanded] = useState(false);
  const [modifyExpanded, setModifyExpanded] = useState(false);
  const [removeExpanded, setRemoveExpanded] = useState(false);
  const [isImage, setIsImage] = useState(false);
  const [values, setValues] = useState({
    sosiaalisuus: 1,
    fysiikka: 1,
    ajattelu: 1
  });
  const [alert, setAlert] = useState({
    showAlert: false,
    success: false,
    alertType: '',
    alertMessage: '',
    closedAlert: false
  });
  const [loadingData, setLoadingData] = useState(null);

  const popupRef = useRef(null);
  const pageRef = useRef(null);

  const setAlertMessage = (alertType) => {
    switch (alertType) {
      case 'success':
        return { message: 'Tehtävä lisätty onnistuneesti!', success: true};
      case 'error':
        return { message: 'Tehtävän lisäys epäonnistui, tarkista tiedot.', success: false };
      case 'removeSuccess':
        return { message: 'Tehtävä poistettu onnistuneesti!', success: true };
      case 'editSuccess':
        return { message: 'Tehtävää muokattu onnistuneesti!', success: true };
      default:
        return 'alertMessage not found';
    }
  };

  const togglePopup = (alertType) => {
    const newAlert = setAlertMessage(alertType);
    setAlert({ ...alert, showAlert: !alert.showAlert, alertType, alertMessage: newAlert.message, success: newAlert.success, closedAlert: alert.showAlert });
  };

  const handleAddClicked = () => {
    if (addExpanded) {
      setAddExpanded(false);
    } else {
      setAddExpanded(true);
      setModifyExpanded(false);
      setRemoveExpanded(false);
    }
  };

  const handleModifyClicked = () => {
    if (modifyExpanded) {
      setModifyExpanded(false);
    } else {
      setModifyExpanded(true);
      setAddExpanded(false);
      setRemoveExpanded(false);
    }
  };

  const handleRemoveClicked = () => {
    if (removeExpanded) {
      setRemoveExpanded(false);
    } else {
      setRemoveExpanded(true);
      setModifyExpanded(false);
      setAddExpanded(false);
    }
  };

  useEffect(() => {
    if (getUser().isAdmin) {
      axios.get(getOrganizationRoute())
        .then(response => {
          setOrganization(response.data.results);
        });
    } else {
      axios.get(`${getOrganizationRoute()}${getUser().userId}`)
        .then(response => {
          setOrganization(response.data.results);
        });
    }

    axios.get(getWorkTimesRoute())
      .then(response => {
        const worktimes = response.data.results
          .map(worktime => ({
            worktime_id: worktime.worktime_id,
            worktime: worktime.worktime,
            selected: false
          }));
        setWorkTimes(worktimes);
      });
  }, []);

  useEffect(() => {
    if (organization.length > 0) {

      setTaskFormData({ ...taskFormData, selectedOrg: organization[0].organization_name });

      setLoadingData(true);
      if (getUser().isAdmin) {
        axios.get(getTaskRoute())
          .then(response => {
            setTasks(response.data.results);
            setLoadingData(false);
          });
      } else {
        axios.get(`${getLocationsRoute()}${organization[0].location_id}`)
          .then(response => {
            setTasks(response.data.results);
            setLoadingData(false);
          });
      }
    }
  }, [organization]);

  useEffect(() => {
    if (alert && alert.showAlert && popupRef.current) {
      popupRef.current.focus();
    }

    if (alert.closedAlert && pageRef && pageRef.current) {
      pageRef.current.focus();
    }
  }, [alert]);

  const fileSelectedHandler = event => {
    const imageFormats = ['jpg', 'png', 'jpeg'];
    const formData = { ...taskFormData };
    if (event.target.files.length >= 1) {
      imageFormats.forEach(format => {
        if (event.target.files[0].type === `image/${format}`) {
          setTaskFormData({ ...formData, selectedFile: event.target.files[0] });
          setIsImage(true);
        }
      });
    } else {
      setIsImage(false);
    }
  };

  const openModal = (key, action) => {
    const task = getTaskData(key, tasks);
    const formData = {
      taskName: task.name,
      taskDescription: task.description,
      ajattelu: task.ajattelu_value,
      sosiaalisuus: task.sosiaalisuus_value,
      fysiikka: task.fysiikka_value,
      location_id: task.location_id,
      selectedFile: task.image,
      selectedOrg: organization[0].organization_name,
    };
    setValues({
      sosiaalisuus: task.sosiaalisuus_value,
      fysiikka: task.fysiikka_value,
      ajattelu: task.ajattelu_value
    });
    const newModalData = {
      name: task.name,
      description: task.description,
      image: task.image,
      action,
      values
    };

    setTaskFormData(formData);
    setTaskOpenedId(key);
    setModalData(newModalData);
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const handleChange = (e) => {
    const formData = { ...taskFormData };
    setTaskFormData(formData);
  };

  const handleValueChange = (category, name, data) => event => {
    const updatedField = { ...data };
    updatedField[`${name}`] = event.target.value;

    if (category === 'task') {
      setTaskFormData(updatedField);
      setValues(updatedField);
    }
  };

  const handleValueChangeForWorkTime = event => {
    const data = workTimes;
    data.forEach(check => {
      if (event.length > 0) {
        event.includes(check) ? check.selected = true : check.selected = false;
      } else {
        check.selected = false;
      }
    });
    setWorkTimes(data);
    console.log('Worktimes:', workTimes);
  };

  const getLocationIdByName = (name) => {
    const locs = [...organization];
    const location = locs
      .find(loc => loc.organization_name === name);
    return location
      ? location.location_id
      : null;
  };


  const getTaskIndex = (data, id) => {
    return data
      .findIndex(task => task.task_id === id);
  };

  const sendTask = () => {

    const data = taskFormData;

    console.log("taskformdata",taskFormData)
    if (data.selectedFile !== null && data.taskName !== '' && data.taskDescription !== '' && isImage) {
      setLoading(true);
      const reader = new FileReader();
      reader.readAsDataURL(taskFormData.selectedFile);
      reader.onload = (e) => {
        const imageData = { file: e.target.result };
        const payload = {
          taskName: taskFormData.taskName,
          taskDescription: taskFormData.taskDescription,
          ajattelu_value: parseInt(taskFormData.ajattelu, 10),
          fysiikka_value: parseInt(taskFormData.fysiikka, 10),
          sosiaalisuus_value: parseInt(taskFormData.sosiaalisuus, 10),
          location_id: getLocationIdByName(taskFormData.selectedOrg),
          image: imageData,
          workTimes
        };
        return axios.post(getTaskRoute(), payload, {
          headers: {
            token: getToken()
          }
        }).then(response => {
          console.log("Response",response);
          setLoading(false);
          togglePopup('success')
        });
      };
    } else {
      togglePopup('error');
    }
  };

  const updateTask = () => {
    setLoading(true);
    const data = taskFormData;
    console.log("taskformdata",taskFormData)
    if (data.selectedFile !== null && data.taskName !== '' && data.taskDescription !== '' && isImage) {
      setLoading(true);
      const reader = new FileReader();
      reader.readAsDataURL(taskFormData.selectedFile);
      reader.onload = (e) => {
        const imageData = { file: e.target.result };
        const payload = {
          taskName: taskFormData.taskName,
          taskDescription: taskFormData.taskDescription,
          ajattelu_value: parseInt(taskFormData.ajattelu, 10),
          fysiikka_value: parseInt(taskFormData.fysiikka, 10),
          sosiaalisuus_value: parseInt(taskFormData.sosiaalisuus, 10),
          location_id: getLocationIdByName(taskFormData.selectedOrg),
          image: imageData,
          workTimes
        };
        axios.put(`${getTaskRoute()}${taskOpenedId}`, payload, {
          headers: {
            token: getToken()
          }
        }).then(response => {
          console.log(response);
          if (response.status === 200) {
            closeModal();
            setLoading(false);
            togglePopup('editSuccess');
          }
        });
      };
    } else {
      togglePopup('error');
    }
  };

  const deleteTask = () => {
    return axios.delete(`${getTaskRoute()}${taskOpenedId}`, {
      headers: {
        token: getToken()
      }
    }).then(response => {
      console.log('result', response);
      if (response.status === 200) {
        closeModal();
        togglePopup('removeSuccess');
      }
    });
  };

  const handleButtonPress = (type, id) => {
    switch (type) {
      case 'post':
        sendTask();
        break;
      case 'modify':
        updateTask();
        break;
      case 'delete':
        deleteTask();
        break;
      default:
        console.log('Something went wrong');
    }
  };

  const tasksToModify = tasks.length > 1 ? tasks.map(task => {
    return (
      <Task
        key={task.task_id}
        image={task.image}
        name={task.name}
        onClick={() => openModal(task.task_id, 'modify')}
      />
    );
  }) : (
    <div>
      <Loader size="lg" />
      <p>Haetaan tehtäviä</p>
    </div>);

  const tasksToDelete = tasks.length > 1 ? tasks.map(task => {
    return (
      <Task
        key={task.task_id}
        image={task.image}
        name={task.name}
        onClick={() => openModal(task.task_id, 'delete')}
      />
    );
  }) : (
    <div>
      <Loader size="lg" />
      <p>Haetaan tehtäviä</p>
    </div>);

  const orgLocation = organization.map(org => {
    return (
      <option key={org.organization_id}>{org.organization_name}</option>
    );
  });
  const addExpandedSymbol = addExpanded ? 'ᐃ' : 'ᐁ';
  const modifyExpandedSymbol = modifyExpanded ? 'ᐃ' : 'ᐁ';
  const removeExpandedSymbol = removeExpanded ? 'ᐃ' : 'ᐁ';

  const style = {
    searchBox: {
      border: '1px solid grey',
      backgroundColor: 'white',
      width: '80%',
      paddingLeft: '1rem'
    },
    chips: {
      background: 'green'
    },
    optionContainer: {
      border: '1px solid',
      color: 'grey'
    },
    option: {
      color: 'black',
      background: 'white'
    },
  };

  return (
    <div className={classes.Container}>
      {alert.showAlert
        && <Popup
          ref={popupRef}
          role="dialog"
          className={alert.success ? 'popup_inner_success' : 'popup_inner_error'}
          text={alert.alertMessage}
          closePopup={togglePopup}
        />}
      <div style={{ display: alert.showAlert ? 'block' : 'none' }} className="DialogOverlay" tabIndex="-1" />
      <Tabs className={classes.Tabs} defaultActiveKey="tasks">
        <Tab className={classes.SingleTab} eventKey="tasks" title="Tehtävät">
          <br />
          <h1 tabIndex="-1" ref={pageRef} className={classes.Text}>Tehtävien hallinta</h1>
          <p className={classes.Text}>
            Tervetuloa tehtävienhallintaan! Voit lisätä, poistaa ja muokata tehtäviä alla olevasta valikosta.
            <br />
            Muista lopuksi tallentaa muutokset.
          </p>
          <TaskContext.Provider value={{
            name: taskFormData.taskName,
            description: taskFormData.taskDescription,
            handleValueChange,
            handleChange,
            location: orgLocation,
            buttonPressedHandler: handleButtonPress,
            fileSelectedHandler,
            data: taskFormData,
            loading,
            values
          }}
          >
            <AdminModal
              modalState={modalOpen}
              name={modalData.name}
              description={modalData.description}
              image={modalData.image}
              closeModal={closeModal}
              values={modalData.values}
              action={modalData.action}
              togglePopup={togglePopup}
            />
            <Accordion defaultActiveKey="0" className={classes.Accordion} style={{ display: 'flex', flexDirection: 'column' }}>
              <Card className={classes.Card} style={{ backgroundColor: '#cbf5a4' }} onClick={() => handleAddClicked()}>
                {getUser().isAdmin ? <div/> :
                <Accordion.Toggle
                  aria-expanded={addExpanded}
                  id="accordion-1"
                  aria-controls="section-1"
                  className={classes.Add}
                  as={Button}
                  variant={Card.Header}
                  eventKey="1"
                  >
                  <p style={{ margin: '0' }} id="add-id">Uusi tehtävä</p>
                  <p aria-hidden="true" style={{ margin: '0' }}>{addExpandedSymbol}</p>
                </Accordion.Toggle>}
                {getUser().isAdmin ? <div/> :
                <Accordion.Collapse id="section-1" aria-labelledby="accordion-1" eventKey="1">
                  <Card.Body className={classes.CardContainer}>
                    <Form>
                      <DescriptionFields defaultName="" defaultDescription="" />
                      <p className={classes.Labels}>
                        Lisää tehtävän kuva
                      </p>
                      <FileUploader />
                      <div aria-labelledby="ajankohdat-label" style={{ marginBottom: '20px' }}>
                        <label
                          htmlFor="select-time-of-day"
                          aria-label="Valitse sijainti. Kirjoita rajataksesi hakua."
                          id="ajankohdat-label"
                          className={classes.Labels}
                          style={{ marginBottom: '0.1rem' }}
                        >
                          Valitse tehtävän ajankohdat
                        </label>
                        <Multiselect
                          id="select-time-of-day"
                          style={style}
                          options={workTimes}
                          onSelect={handleValueChangeForWorkTime}
                          onRemove={handleValueChangeForWorkTime}
                          displayValue="worktime"
                          placeholder="Valitse tai kirjoita"
                          closeOnSelect={false}
                          showCheckbox={false}
                          closeIcon="close"
                          avoidHighlightFirstOption={true}
                        />
                      </div>
                      <p className={classes.Labels}>
                        Anna tehtävän attribuutit. 1 ollessa pienin ja 5 ollessa suurin
                      </p>
                      <ValueSelectors ajattelu={taskFormData.ajattelu} sosiaalisuus={taskFormData.sosiaalisuus} fysiikka={taskFormData.fysiikka} />
                      <Submitter buttonClass={classes.ButtonAdd} variant="success" type="post" buttonText="Lisää tehtävä" />
                    </Form>
                  </Card.Body>
                </Accordion.Collapse>
                }
              </Card>
              <Card className={classes.Card} style={{ backgroundColor: '#ffe38f' }} onClick={() => handleModifyClicked()}>
                <Accordion.Toggle
                  aria-expanded={modifyExpanded}
                  className={classes.Modify}
                  as={Button}
                  variant={Card.Header}
                  eventKey="2"
                >
                  <p style={{ margin: '0' }}>Muokkaa tehtävää</p>
                  <p className="screenreader" aria-hidden={!loadingData}>Ladataan tehtäviä</p>
                  <p aria-hidden="true" style={{ margin: '0' }}>{modifyExpandedSymbol}</p>
                </Accordion.Toggle>
                <Accordion.Collapse id="modify-id" eventKey="2" aria-hidden={loadingData}>
                  <Card.Body className={classes.CardContainer}>
                    <Row style={{ color: 'black', textAlign: 'center' }}>
                      {tasksToModify}
                    </Row>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card className={classes.Card} style={{ backgroundColor: '#ff8787' }} onClick={() => handleRemoveClicked()}>
                <Accordion.Toggle
                  aria-expanded={removeExpanded}
                  className={classes.Remove}
                  as={Button}
                  variant={Card.Header}
                  eventKey="3"
                >
                  <p style={{ margin: '0' }}>Poista tehtävä</p>
                  <p className="screenreader" aria-hidden={!loadingData}>Ladataan tehtäviä</p>
                  <p aria-hidden="true" style={{ margin: '0' }}>{removeExpandedSymbol}</p>
                </Accordion.Toggle>
                <Accordion.Collapse id="remove-id" eventKey="3" aria-hidden={loadingData}>
                  <Card.Body className={classes.CardContainer}>
                    <Row style={{ color: 'black', textAlign: 'center' }}>
                      {tasksToDelete}
                    </Row>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </TaskContext.Provider>
        </Tab>
        <Tab className={classes.Tab} eventKey="help" title="Ohjeet">
          <AdminInstructions />
        </Tab>
      </Tabs>
    </div>
  );
};

export default AdminPage;
