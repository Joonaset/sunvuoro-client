import React, { useRef, useEffect } from 'react';
import {
  Modal, Image
} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';

import DescriptionFields from '../../components/Forms/Tasks/TaskDescriptionFields';
import FileUploader from '../../components/Forms/Tasks/TaskFileUploader';
import ValueSelectors from '../../components/Forms/Tasks/TaskValueSelectors';
import Submitter from '../../components/Forms/Tasks/TaskSubmitter';

const AdminModal = (props) => {
  const modalRef = useRef(null);

  useEffect(() => {
    if (modalRef && modalRef.current) {
      modalRef.current.focus();
    }
  }, [props.modalState]);

  const buttonText = props.action === 'modify' ? 'Tallenna muutokset' : 'Poista tehtävä';
  const buttonColor = props.action === 'modify' ? 'warning' : 'danger';
  const content = props.action === 'modify' ?
    <Modal
      show={props.modalState}
      dialogClassName="modal-width"
      onHide={props.closeModal}
      aria-modal="true"
      aria-label="Muokkaa tehtävää -ikkuna"
    >
      <Modal.Header closeButton>
        <Modal.Title id="modal-title">
          <h1>{`Muokkaa tehtävää: ${props.name}`}</h1>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form style={{ margin: '2vw' }}>
          <DescriptionFields ref={modalRef} defaultName={props.name} defaultDescription={props.description} />
          <FileUploader />
          <Image alt={`Työtä '${props.name}' esittävä kuva`} src={props.image} style={{ maxHeight: '60%', maxWidth: '69%', marginTop: '2vw', marginBottom: '2vw' }} />
          <h2 style={{ fontSize: '1.3rem' }}>Tehtävän laadulliset painotukset</h2>
          <ValueSelectors ajattelu={props.values.ajattelu} sosiaalisuus={props.values.sosiaalisuus} fysiikka={props.values.fysiikka} />
          <Submitter variant={buttonColor} type={props.action} buttonText={buttonText}/>
        </Form>
      </Modal.Body>
    </Modal>
    :
    <Modal
      show={props.modalState}
      size="lg"
      onHide={props.closeModal}
      style={{ maxWidth: '100%', top: '0', left: '0', right: '0', bottom: '0' }}
      aria-modal="true"
      aria-label="Poista tehtävä -ikkuna"
    >
      <Modal.Header closeButton>
        <Modal.Title id="modal-title">
          {`Poista tehtävä: ${props.name}`}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form style={{ margin: '2vw' }}>
          <p>Oletko varma että haluat poistaa tehtävän?</p>
          <Submitter ref={modalRef} variant={buttonColor} type={props.action} buttonText={buttonText} />
        </Form>
      </Modal.Body>
    </Modal>;
  return (
    content
  );
};

export default AdminModal;
