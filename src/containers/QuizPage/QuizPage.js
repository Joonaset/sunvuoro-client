import React, { useState, useEffect, useRef } from 'react';
import { Container, Row, Button, Col, Nav } from 'react-bootstrap';
import axios from 'axios';
import { Multiselect } from 'multiselect-react-dropdown';
import Category from '../../components/Categories/Category';
import Task from '../../components/TaskCard/Task';
import TaskModal from '../../components/TaskCard/TaskModal';
import { getTaskRoute, getDataCollectionRoute, getCitiesRoute, getWorkTimesRoute } from '../../utils/Api';
import { getTaskData } from '../../utils/DataUtils';
import Loader from '../../components/Loader';
import classes from './QuizPage.module.css';
import addImage from '../../images/add_new_task.png';
import { getSliderImg } from '../../utils/SliderImgUtils';
import Popup from '../../components/Popup/Popup';

const QuizPage = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState([
    { name: 'physicality', value: 3, label: 'Fyysisyys', desc: 'Kuntoa nostavaa, kehoa vahvistavaa tekemistä' },
    { name: 'sociability', value: 3, label: 'Sosiaalisuus', desc: 'Ihmisten kanssa vuorovaikuttavaa tekemistä' },
    { name: 'problemsolving', value: 3, label: 'Pulmanratkaisu', desc: 'Pohdintaa ja hoksottimia vaativaa tekemistä' }
  ]);
  const [tasks, setTasks] = useState([
    {
      task_id: 0,
      name: '',
      description: '',
      fysiikka_value: 3,
      sosiaalisuus_value: 3,
      ajattelu_value: 3,
      image: '',
      city_name: '',
      matchingValue: 0,
      lat: 0.0,
      longitude: 0.0,
      selected: false
    }
  ]);

  const [cities, setCities] = useState([
    {
      city_name: '',
      city_id: 0,
      selected: false
    }
  ]);

  const [worktimes, setWorktimes] = useState([
    {
      worktime: '',
      worktime_id: 0,
      selected: true
    }
  ]);

  const [taskWorktimeIds, setTaskworktimeIds] = useState([
    {
      task_id: 0,
      worktime_id: 0
    }
  ]);

  const [modalData, setModalData] = useState({
    name: '',
    task_id: 0,
    description: '',
    image: '',
    imageAlt: '',
    address: '',
    postcode: '',
    task_when: '',
    organization_name: '',
    link: '',
    phone: '',
    email: '',
    applicationSender: '',
    applicationEmail: '',
    applicationBody: '',
    longitude: 0.0,
    latitude: 0.0,
    fysiikka: 0,
    sosiaalisuus: 0,
    ajattelu: 0
  });

  const titleRef = useRef(null);
  const popupRef = useRef(null);
  const pageRef = useRef(null);

  const [alert, setAlert] = useState({
    showAlert: false,
    success: false,
    alertType: '',
    alertMessage: '',
    closedAlert: false
  });

  useEffect(() => {
    if (modalOpen && titleRef && titleRef.current) {
      titleRef.current.focus();
    }
  }, [modalOpen]);

  useEffect(() => {
    if (alert && alert.showAlert && popupRef.current) {
      popupRef.current.focus();
    }

    if (alert.closedAlert && pageRef && pageRef.current) {
      pageRef.current.focus();
    }
  }, [alert]);

  useEffect(() => {
    const fetchCitiesData = async () => {
      const citiesData = await axios.get(getCitiesRoute());
      console.log("citiesData",citiesData)
      setCities(citiesData.data.results);
    };

    const fetchWorktimesData = async () => {
      const worktimesData = await axios.get(getWorkTimesRoute());
      console.log("workTimesData:",worktimesData)
      setWorktimes(worktimesData.data.results);
    };

    const fetchTaskWorktimeIds = async () => {
      const ids = await axios.get(`${getTaskRoute()}taskWorktimes`);
      console.log("ids",ids)
      setTaskworktimeIds(ids.data.results);
    };

    fetchCitiesData();
    fetchWorktimesData();
    fetchTaskWorktimeIds();
  }, []);

  useEffect(() => {
    setTasks(sortTasks(valuateTasks(tasks)));
  }, [categories]);

  useEffect(() => {
    setTasks(filterTasks(tasks));
  }, [cities]);

  useEffect(() => {
    setTasks(filterTasks(tasks));
  }, [worktimes]);

  const getSliderValues = () => {
    const physicalityValue = categories[0].value;
    const sociabilityValue = categories[1].value;
    const problemsolvingValue = categories[2].value;
    return {
      fysiikka_value: physicalityValue,
      sosiaalisuus_value: sociabilityValue,
      ajattelu_value: problemsolvingValue
    };
  };

  const valuateTasks = (tasksToValuate) => {
    const values = getSliderValues();
    const maxMatch = 12;

    const modifiedTasks = tasksToValuate.map(task => {
      const physDiff = task.fysiikka_value - values.fysiikka_value;
      const socDiff = task.sosiaalisuus_value - values.sosiaalisuus_value;
      const probDiff = task.ajattelu_value - values.ajattelu_value;
      const difference = Math.abs(physDiff) + Math.abs(socDiff) + Math.abs(probDiff);
      return { ...task, matchingValue: maxMatch - difference };
    });


    return modifiedTasks;
  };

  const sortTasks = (tasksToSort) => tasksToSort.sort((a, b) => ((a.matchingValue > b.matchingValue) ? -1 : 1));

  const filterTasks = (tasksToFilter) => {
    const halfComparedTasks = compareTasksToCities(tasksToFilter);
    const fullyComparedTasks = compareTasksToTimes(halfComparedTasks);
    return fullyComparedTasks;
  };

  const compareTasksToCities = (tasksToCompare) => {
    const filtered = [...tasksToCompare];
    filtered.forEach(task => {
      cities.forEach((city) => {
        if (city.city_name === task.city_name) {
          task.selected = city.selected;
        }
      });
    });
    return filtered;
  };

  const compareTasksToTimes = (tasksToCompare) => {
    const ids = [...taskWorktimeIds];
    const filteredTasks = [...tasksToCompare];
    ids.forEach(id => {
      worktimes.forEach(worktime => {
        if (id.worktime_id === worktime.worktime_id) {
          filteredTasks.forEach(task => {
            if (task.task_id === id.task_id && task.selected && !worktime.selected) {
              task.selected = false;
            }
          });
        }
      });
    });

    return filteredTasks;
  };

  const openModal = (key) => {
    const task = getTaskData(key, tasks);
    setModalData(task);
    setModalOpen(true);
    axios.put(`${getDataCollectionRoute()}taskClicks/${task.task_id}`);
  };

  const changeCategoryValue = (category, val) => {
    const oldCategories = [...categories];

    const changedCategories = oldCategories
      .map(cat => (cat.name === category
        ? { ...cat, value: val }
        : cat));

    setCategories(changedCategories);
  };

  const handleCityChange = event => {
    const oldCities = [...cities];
    const selectedCities = event.map(e => e.city_id);

    const changedCities = oldCities.map(city => ((selectedCities.includes(city.city_id))
      ? { ...city, selected: true }
      : { ...city, selected: false }));

    setCities(changedCities);
  };

  const handleTimeChange = event => {
    const oldTimes = [...worktimes];
    const selectedTimes = event.map(e => e.worktime_id);

    const changedTimes = oldTimes.map(time => ((selectedTimes.includes(time.worktime_id))
      ? { ...time, selected: true }
      : { ...time, selected: false }));

    setWorktimes(changedTimes);
  };

  const handlePhysicalityChange = (val) => {
    changeCategoryValue('physicality', val);
  };

  const handleSociabilityChange = (val) => {
    changeCategoryValue('sociability', val);
  };

  const handleProblemsolvingChange = (val) => {
    changeCategoryValue('problemsolving', val);
  };

  const handleValueChange = name => event => {
    const data = { ...modalData };
    data[`${name}`] = event.target.value;
    setModalData(data);
  };

  const sendSearchValues = () => {
    const values = getSliderValues();
    axios
      .post(`${getDataCollectionRoute()}searchValues`, values);
  };

  const getTasks = () => {
    sendSearchValues();
    setLoading(true);
    axios
      .get(getTaskRoute())
      .then((response) => {
        setTasks(sortTasks(valuateTasks(filterTasks(response.data.results))));
        setLoading(false);
      });
  };

  const getWorktimeNames = taskId => {
    let workTimesString = ""
    if(taskWorktimeIds != null) {
      taskWorktimeIds.map(task => {
        worktimes.map(worktime => {
          if (task.task_id === taskId && task.worktime_id == worktime.worktime_id) {
            workTimesString += worktime.worktime + ", "
          }
        })
      })
      return workTimesString.substring(0,workTimesString.length-2);
    }
  }

  const tasksUi = tasks
    .filter((task) => task.selected)
    .map(task => <Task
      key={task.task_id}
      image={task.image}
      imageAlt={task.image_alt || `Työtä '${task.name}' esittävä kuva`}
      name={task.name}
      city={task.city_name}
      address={task.address}
      postcode={task.postcode}
      task_when={task.task_when}
      organization_name={task.organization_name}
      link={task.link}
      phone={task.phone}
      email={task.email}
      fysiikkaValue={task.fysiikka_value}
      sosiaalisuusValue={task.sosiaalisuus_value}
      ajatteluValue={task.ajattelu_value}
      onClick={() => openModal(task.task_id)}
    />);

  const topTasks = tasksUi.slice(0, 3);
  const allTasks = tasksUi
    .slice(3)
    .map((task, i) => (i > 0 && i % 10 === 0
      ? <Task
        key={i}
        image={addImage}
        imageAlt="Ehdota tehtävää"
        name="Ehdota tehtävää"
        city="Etkö löytänyt mieleistäsi tehtävää? Ehdota tästä!"
        address=""
        postcode=""
        task_when=""
        organization_name=""
        fysiikkaValue=""
        sosiaalisuusValue=""
        ajatteluValue=""
        onClick={() => openModal('add')}
      />
      : task));

  const fetchButton = <Button className={classes.searchButton} onClick={getTasks} disabled={loading}>{loading ? <Loader size="lg" /> : 'Hae!'}</Button>;
  const allTasksLink = <a href="/tasks" className={classes.allTasksLink}>Siirry kaikki tehtävät -sivulle</a>;

  const placeholder = tasks.length > 1
    ? <div className={classes.tasksContainer}>
      <Row style={{ borderBottom: '1vw solid #00000085', justifyContent: 'center', fontSize: '2rem', margin: '2vh 1vw 2vh 1vw', backgroundColor: 'white' }}>
        <Row style={{ justifyContent: 'center', fontSize: '1.8rem', marginTop: '3vh', color: '#008b00', maxWidth: '90vw' }}>
          <h3>Parhaiten sinulle soveltuvat tehtävät:</h3>
        </Row>
        <Row className={classes.TopTasks}>
          {topTasks}
        </Row>
      </Row>
      <Row style={{ justifyContent: 'center', fontSize: '1.5rem', marginTop: '3vh' }}>
        <h4>Muita tehtäviä:</h4>
      </Row>
      <Row style={{ maxWidth: '90vw', justifyContent: 'center' }}>
        {allTasks}
      </Row>
    </div>
    : <div />;

  const sendEmail = () => {
    const applicationData = {
      name: modalData.applicationSender,
      email: modalData.applicationEmail,
      bodyText: modalData.applicationBody,
      taskName: modalData.name
    };
    axios.post(getTaskRoute()+"application",applicationData)
      .then(result => console.log('result:', result));

    axios.put(getDataCollectionRoute()+`contactClicks/${modalData.task_id}`)
      .then(result => console.log('result:', result));

    togglePopup('success');
    // close modal after sending email
    setModalOpen(false);
  };

  const setAlertMessage = (alertType) => {
    switch (alertType) {
      case 'success':
        return { message: 'Viesti lähetetty onnistuneesti, olemme teihin yhteydessä mahdollisimman pian!', success: true};
      case 'error':
        return { message: 'Viestin lähetys epäonnistui!', success: false };
      default:
        return 'alertMessage not found';
    }
  };

  const togglePopup = (alertType) => {
    const newAlert = setAlertMessage(alertType);
    setAlert({ ...alert, showAlert: !alert.showAlert, alertType, alertMessage: newAlert.message, success: newAlert.success });
  };

  const showAlert = alert.showAlert
    ? <Popup
      role="dialog"
      ref={popupRef}
      className={alert.success ? 'popup_inner_success' : 'popup_inner_error'}
      text={alert.alertMessage}
      closePopup={() => setAlert({ ...alert, showAlert: false, closedAlert: alert.showAlert })}
    />
    : <div />;

  return (
    <Container className={classes.Container}>
      {showAlert}
      <div style={{ display: alert.showAlert ? 'block' : 'none' }} className="DialogOverlay" tabIndex="-1" />
      <TaskModal
        modalState={modalOpen}
        name={modalData.name}
        description={modalData.description}
        image={modalData.image}
        imageAlt={modalData.imageAlt || `Työtä '${modalData.name}' esittävä kuva`}
        city={modalData.city_name}
        address={modalData.address}
        postcode={modalData.postcode}
        task_when={modalData.task_when}
        organization_name={modalData.organization_name}
        email={modalData.email}
        link={modalData.link}
        phone={modalData.phone}
        workTimes={getWorktimeNames(modalData.task_id)}
        closeModal={() => setModalOpen(false)}
        handleValueChange={handleValueChange}
        latitude={modalData.latitude}
        longitude={modalData.longitude}
        titleRef={titleRef}
        sendEmail={() => sendEmail()}
        fysiikka_value={modalData.fysiikka_value}
        sosiaalisuus_value={modalData.sosiaalisuus_value}
        ajattelu_value={modalData.ajattelu_value}
      />
      <Row className={classes.Categories}>
        <Row className={classes.Description}><h1 tabIndex="-1" ref={pageRef}>Hae tekemistä</h1></Row>
        <Row className={classes.AfterDescription} />
        <Row style={{ display: 'flex', flexDirection: 'row' }}>
          <div className={classes.CategoryItem}>
            <img
              className={classes.Images}
              src={getSliderImg(0, categories[0].value)}
              alt=""
            />
            <Category
              label={categories[0].label}
              desc={categories[0].desc}
              catValue={categories[0].value}
              handleChange={handlePhysicalityChange}
            />
          </div>
          <div className={classes.CategoryItem}>
            <img
              className={classes.Images}
              src={getSliderImg(1, categories[1].value)}
              alt=""
            />
            <Category
              label={categories[1].label}
              desc={categories[1].desc}
              catValue={categories[1].value}
              handleChange={handleSociabilityChange}
            />
          </div>
          <div className={classes.CategoryItem}>
            <img
              className={classes.Images}
              src={getSliderImg(2, categories[2].value)}
              alt=""
            />
            <Category
              label={categories[2].label}
              desc={categories[2].desc}
              catValue={categories[2].value}
              handleChange={handleProblemsolvingChange}
            />
          </div>
        </Row>
      </Row>
      <Row className={classes.FilterContainer}>
        <Col className={classes.SingleFilter}>
          <div aria-labelledby="sijainti-label">
            <h2 id="sijainti-label">Kaupungit</h2>
            <label htmlFor="select-location" aria-label="Valitse sijainti. Kirjoita rajataksesi hakua." style={{ marginBottom: '0.1rem' }}>
              Valitse sijainti
            </label>
            <Multiselect
              id="select-location"
              options={cities}
              displayValue="city_name"
              placeholder="Valitse tai kirjoita"
              onSelect={handleCityChange}
              onRemove={handleCityChange}
              emptyRecordMsg="Dataa ei löytynyt"
              closeOnSelect={false}
              showCheckbox={false}
              closeIcon="close"
              avoidHighlightFirstOption={true}
            />
          </div>
        </Col>
        <Col className={classes.SingleFilter}>
          <div aria-labelledby="tyoajat-label">
            <h2 id="tyoajat-label">Tehtävän ajankohdat</h2>
            <label htmlFor="select-time-of-day" aria-label="Valitse ajankohta. Kirjoita rajataksesi hakua." style={{ marginBottom: '0.1rem' }}>
              Tehtävän ajankohdat
            </label>
            <Multiselect
              id="select-time-of-day"
              options={worktimes}
              displayValue="worktime"
              placeholder="Valitse tai kirjoita"
              onSelect={handleTimeChange}
              onRemove={handleTimeChange}
              emptyRecordMsg="Dataa ei löytynyt"
              closeOnSelect={false}
              showCheckbox={false}
              closeIcon="close"
              avoidHighlightFirstOption={true}
            />
          </div>
        </Col>
      </Row>
      {fetchButton}
      <div style={{ paddingBottom: '1rem' }}>
        {allTasksLink}
      </div>
      {placeholder}
    </Container>
  );
};

export default QuizPage;
