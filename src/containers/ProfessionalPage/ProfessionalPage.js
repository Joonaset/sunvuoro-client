import React, { Component } from 'react';
import { Container, Row, Button, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import axios from 'axios';
import { getLoginRoute } from '../../utils/Api';
import { getUser, getToken, setUserSession } from '../../utils/AuthUtils';
import Accordion from 'react-bootstrap/Accordion'
import { Card } from 'react-bootstrap';
import Login from '../../components/Login';
import classes from '../ProfessionalPage/ProfessionalPage.module.css';
import {changeDivDisplayStyle} from '../../utils/DivUtils';
import Alert from 'react-bootstrap/Alert'

class ProfessionalPage extends Component {
    state = {
      tasks: [{
        taskId: 0,
        name: '',
        description: ''
      }],
      username: '',
      password: '',
      loggedIn: false,
      loginFailed: false,
      accordion1Expanded: false,
      accordion2Expanded: false,
      accordion3Expanded: false
    }

    componentDidMount() {
      getToken() !== null ? this.setState({loggedIn: true}) : this.setState({loggedIn: false})
    }

    handleChange = name => event => {
      let updatedField = {};
      updatedField[`${name}`] = event.target.value;
      this.setState(updatedField);
    }

    handleLogin = () => {
      const headers = { 'Access-Control-Allow-Origin': '*'};

      axios.post(getLoginRoute(),
      { headers, username: this.state.username,
                                    password: this.state.password,
                                    grant_type: 'password',
                                    client_id: null,
                                    client_secret: null })
            .then(response => {
              if(response.data.loginSuccess === true) {
                setUserSession(response.data.token, response.data.user);
                this.setState({loginFailed: false, loggedIn: true})
                changeDivDisplayStyle("adminpane",true)
                this.props.setUpdateAdminNav(true);
              }
              else {
                this.setState({loginFailed: true, loggedIn: false})
              }
            }).catch(() => {
              this.setState({loginFailed: true})
            })

      this.forceUpdate();
    }

    render() {
      const hyvinvoinnintilatLink = <a href="https://hyvinvoinnintilat.fi/" target="_blank" className={classes.Link}> hyvinvoinnintilat.fi</a>;
      const loginForm = this.state.loggedIn ?
      <div style={{backgroundColor:'#d4edda', color: '#155724', padding:'2rem'}} className={classes.loginSuccess}>
        <h1 style={{ paddingBottom: '1rem' }}>Olet kirjautunut sisään</h1>
        <p>Hei <i>{getUser().username}</i>. Tervetuloa Sun vuoro -palveluun! Aloita siirtymällä hallintapaneelin alasivuille alla olevista painikkeista tai navigointipalkin
        pudotusvalikosta.</p>
        <div style={{ borderBottom: '1px solid grey', marginBottom: '1.5rem' }}></div>
        <h2>Hallintapaneeli</h2>
        <ul className={classes.ControlPanelLinks}>
          <li>
            <LinkContainer to="/admin" exact>
              <Nav.Link className={classes.navLink}>Tehtävien hallinta</Nav.Link>
            </LinkContainer>
          </li>
          <li>
            <LinkContainer to="/organizations" exact>
              <Nav.Link className={classes.navLink}>Organisaatioiden hallinta</Nav.Link>
            </LinkContainer>
          </li>
          <li>
            {getUser().isAdmin
            ? <LinkContainer to="/applications" exact>
                <Nav.Link className={classes.navLink}>Hakemusten hallinta</Nav.Link>
              </LinkContainer>
            : <div />}
          </li>
          <li>
            <LinkContainer to="/stats" exact>
              <Nav.Link className={classes.navLink}>Tilastot</Nav.Link>
            </LinkContainer>
          </li>
        </ul>
      </div>
      :
      <Login username={this.state.username}
             password={this.state.password}
             usernameChange={() => this.handleChange('username')}
             passwordChange={() => this.handleChange('password')}
             login={() => this.handleLogin}/>
      const loginFailed = this.state.loginFailed ?  <Alert variant='danger' className={classes.Loginalert}> Kirjautuminen epäonnistui! Tarkista salasana ja/tai käyttäjätunnus. </Alert> : <p></p>
      return(
        <Container className={classes.Container}>
        <div className={classes.DescriptionContainer}>
          <h1 style={{textAlign:'center', padding:'1rem'}}>Tietoa sivustosta</h1>
          <Accordion defaultActiveKey="0" style={{display:'flex', flexDirection:'column'}}>
            <Card className={classes.DescriptionCard}>
              <h2>
                <Accordion.Toggle
                  aria-expanded={this.state.accordion1Expanded}
                  onClick={() => this.setState({ accordion1Expanded: !this.state.accordion1Expanded })}
                  id="accordion-1"
                  aria-controls="section-1"
                  className={classes.Accord}
                  as={Button}
                  variant={Card.Header}
                  eventKey="1"
                >
                  Mikä on Sun Vuoro?
                </Accordion.Toggle>
              </h2>
              <Accordion.Collapse id="section-1" aria-labelledby="accordion-1" eventKey="1">
                <Card.Body className={classes.CardContainer}>
                Sun vuoro on työkalu, jonka avulla tarjoamme sinulle sopivia tehtäviä Hyvinvoinnin tilassa lähellä sinua.
                Hyvinvoinnin tiloja on tällä hetkellä neljällä paikkakunnalla. Niistä saat lisätietoa {hyvinvoinnintilatLink}.<br/><br/>
                Ajatuksena on, että yhteisössä merkitykselliseen, itse valittuun, sopivasti mitoitettuun
                toimintaan/tehtävään osallistumalla ihmisen hyvinvointi ja tyytyväisyys lisääntyvät.
                <br/><br/>
                Toimintaan osallistumalla osallisuus yhteisön toimintaan kasvaa ja osallistuja voi koetella kykyjään ja taitojaan.
                <br/>
                Syrjäytyessä osallisuus vähenee ja kykyjen sekä taitojen koettelu kapeutuvat. Kynnys toimintaan osallistumiseen nousee.
                <br></br>
                <br></br>
                <div>
                  <h3 style={{textAlign:'center'}}>Sun Vuoro</h3>
                  <Row>
                  <ul style={{textAlign:'left'}}>
                    <li>madaltaa kynnystä lähteä fyysisesti toiminnan äärelle </li>
                    <li>kohtauttaa toiminnan vahvuuksia ja haasteita Hyvinvoinnin tilan tehtäviin (mätsäys)
                      tarjoamalla tehtävää, joka on kuvattu ja jota voi lähteä kokeilemaan sovittuna aikana </li>
                    <li>mahdollistaa tilan muiden tehtävien tutkimisen </li>
                    <li>antaa mahdollisuutta itsearviointiin laajemmin (motivaation parantuminen oman
                      kyvykkyyden hahmottamisen kautta) </li>
                  </ul>
                  </Row>
                </div>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card className={classes.DescriptionCard}>
              <h2>
                <Accordion.Toggle
                  aria-expanded={this.state.accordion2Expanded}
                  onClick={() => this.setState({ accordion2Expanded: !this.state.accordion2Expanded })}
                  id="accordion-2"
                  aria-controls="section-2"
                  className={classes.Accord}
                  as={Button}
                  variant={Card.Header}
                  eventKey="3"
                >
                  Ihminen syntyy tekijäksi
                </Accordion.Toggle>
              </h2>
              <Accordion.Collapse id="section-2" aria-labelledby="accordion-2" eventKey="3">
                <Card.Body className={classes.CardContainer}>
                  Vasta noin 2-vuotiaana otamme kielen ja puhumisen avuksemme. Toimintaan osallistuminen eli tehtävien tekeminen tuottaa
                  hyvinvointia silloin, kun tekeminen on mitoitettu oikein tekijän tarpeisiin ja tekemisessä on niitä ulottuvuuksia,
                  joista tekijä on kiinnostunut ja joita hän sillä hetkellä elämässään tarvitsee.
                  <br></br>
                  <br></br>
                  <a href="https://hyvinvoinnintilat.fi/" className={classes.Link} target="_blank" > Hyvinvoinnin tilat</a> tarjoavat tekemistä, joilla pääsee aloittamaan osallistumista tilan toimintaan omien tarpeiden mukaisesti.
                  <br></br>
                  <br></br>
                  Yksi tehtävä ei ole laaja tehtävien kokonaisuus, kuten esimerkiksi työ usein on. Tehtävä on ikään kuin irrotettu
                  suuremmasta kokonaisuudesta. Tavoitteena on, että yksinkertaisen tekemisen kautta tilan toimintaan pääsee matalalla
                  kynnyksellä mukaan ja itsearvioiduilla ulottuvuuksilla voi parantaa omaa hyvinvointiaan.
                  <br></br>
                  <br></br>
                  Esimerkiksi opettaja, joka on päivisin paljon sosiaalisissa kontakteissa, haluaa ryhtyä vapaaehtoiseksi
                  tapahtumien apukädeksi. Hän toivoo tehtävältä fyysistä ponnistelua mieluummin kuin sosiaalisuutta tai pulmanratkaisua.
                  Tai opiskelija, joka on yksinäinen ja alkaa karttaa ihmiskontakteja, voi tarvita itsensä altistamista sosiaalisiin
                  tilanteisiin. Tai henkilö, jolla on ilmaisemisen hankaluutta ja liikuntarajoite, voi kaivata elämäänsä osallistumista
                  ongelmien ratkaisun parissa.
                </Card.Body>
              </Accordion.Collapse>
            </Card>

            <Card className={classes.DescriptionCard}>
              <h2>
                <Accordion.Toggle
                aria-expanded={this.state.accordion3Expanded}
                onClick={() => this.setState({ accordion3Expanded: !this.state.accordion3Expanded })}
                id="accordion-3"
                aria-controls="section-3"
                className={classes.Accord}
                as={Button}
                variant={Card.Header}
                eventKey="5"
              >
                Hae mukaan!
                </Accordion.Toggle>
              </h2>
              <Accordion.Collapse id="section-3" aria-labelledby="accordion-3" eventKey="5">
                <Card.Body className={classes.RegisterCardContainer}>
                  <p>
                    Kiinnostuitko? Nappia painamalla voit liittää organisaatiosi osaksi SunVuoroa!
                  </p>
                  <p>
                    Klikkaa
                    <Button className="sun-vuoro-btn" style={{width:'fit-content', alignSelf:'center', margin:'0.5rem'}} href="/register">
                      Rekisteröidy
                    </Button>
                    -painiketta ja täytä hakemus niin olemme sinuun yhteydessä pikimmiten!
                </p>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </div>
        <div  onKeyPress={e => {
          if (e.charCode === 13) {
            this.handleLogin()
          }
        }}className={classes.LoginContainer}>
         {loginForm}
         {loginFailed}
        </div>
        </Container>
      );
    }
  }

  export default ProfessionalPage;
