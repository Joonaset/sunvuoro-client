import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import ProfessionalPage from './ProfessionalPage';

it('ProfessionalPage should not have basic accessibility issues', async () => {
  const { container } = render(<ProfessionalPage />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
