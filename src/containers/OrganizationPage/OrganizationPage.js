import React, {Component} from 'react'
import classes from './OrganizationPage.module.css';
import Accordion from 'react-bootstrap/Accordion'
import { Card } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import axios from 'axios';
import { getOrganizationRoute} from '../../utils/Api';
import { getUser,getToken } from '../../utils/AuthUtils';
import { Multiselect } from 'multiselect-react-dropdown';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

class OrganizationPage extends Component {
  state = {
      modifyExpanded: false,
      removeExpanded: false,
      organizationData: [{
        organization_id: 0,
        organization_name: '',
        email: '',
        phone: '',
        link: '',
        city_name: '',
        address: '',
        postcode: '',
        location_id: 0
      }],
        modifyAccordionExpanded: false,
        removeAccordionExpanded: false,
        selectedOrganizationData: [{
          organization_id: 0,
          organization_name: '',
          email: '',
          phone: '',
          link: '',
          city_name: '',
          address: '',
          postcode: '',
          location_id: 0
      }],
        organizationDropdown: [{
          organization_name: ''
      }]
  }

  handleValueChange = name => event => {
      const data = {...this.state.selectedOrganizationData}
      data[`${name}`] = event.target.value;
      this.setState({
          selectedOrganizationData: data
        })
  }

  handleModifyClicked = () => {
    if (this.state.modifyExpanded) {
      this.setState({modifyExpanded: false})
    } else {
      this.setState({modifyExpanded: true})
      this.setState({removeExpanded: false})
    }
  }

  handleRemoveClicked = () => {
    if (this.state.removeExpanded) {
      this.setState({removeExpanded: false})
    } else {
      this.setState({removeExpanded: true})
      this.setState({modifyExpanded: false})
    }
  }

  handleButtonPress = () => {
    console.log("button presses")
    axios.post(getOrganizationRoute()+"/updateOrganization",this.state.selectedOrganizationData,{
      headers: {
        token: getToken()
      }
    })
    .then(
      results => {
        console.log(results)
      }
    )
  }

  handleOrganizationChange = (selectedOrg) => {
    const orgData = this.state.organizationData
    const orgMatch = orgData.find(org => org.organization_name === selectedOrg[0].name)
    this.setState({
      selectedOrganizationData: orgMatch
    })
  }


  async componentDidMount() {
      if (getUser().isAdmin) {
        axios.get(getOrganizationRoute())
      .then(response => {
          this.setState({organizationData: response.data.results})
          console.log("Response",response)
          const options = this.state.organizationData.map(org => {
            return { name: org.organization_name}
          })
          this.setState({
            organizationDropdown: options
          })
        })
      }
      else {
        axios.get(getOrganizationRoute()+getUser().userId)
      .then(response => {
          this.setState({organizationData: response.data.results})
          console.log("Response",response)
          const options = this.state.organizationData.map(org => {
            return { name: org.organization_name}
          })
          this.setState({
            organizationDropdown: options
          })
        })
      }
  }

  render() {
    let modifyExpandedSymbol = this.state.modifyExpanded ? 'ᐃ' : 'ᐁ';
    let removeExpandedSymbol = this.state.removeExpanded ? 'ᐃ' : 'ᐁ';
    return (
      <div className={classes.Container}>
        <div className={classes.SingleTab} eventkey="maintainer" title="Paikat">
          <br></br>
          <h1 className={classes.OrgText}>Organisaatioiden hallinta</h1>
          <p className={classes.OrgText}>
            Valitse ensin organisaatio listalta. Sen jälkeen voit muokata organisaation tietoja.
          </p>
          <p className={classes.OrgText}>
            Jos haluat poistaa organisaation, ota yhteyttä sivuston ylläpitäjään.
          </p>
          <p className={classes.OrgText}>
            Muista lopuksi tallentaa muutokset.
          </p>
          <div aria-labelledby="organisaatio-label" className={classes.OrgSelectorContainer}>
            <label htmlFor="select-organization" aria-label="Valitse organisaatio. Kirjoita rajataksesi hakua." id="organisaatio-label" style={{ marginBottom: '0.1rem' }}>
              Valitse organisaatio
            </label>
              <Multiselect
                id="select-organization"
                options={this.state.organizationDropdown}
                placeholder="Valitse organisaatio"
                displayValue="name"
                singleSelect={true}
                onSelect={this.handleOrganizationChange}
              />
          </div>
          <br></br>
          <Accordion className={classes.OrgAccord} defaultActiveKey="0">
            <Card className={classes.Card} style={{backgroundColor:'#ffe38f'}} onClick={() => this.handleModifyClicked()}>
              <Accordion.Toggle
                aria-expanded={this.state.modifyExpanded}
                id="accordion-1"
                aria-controls="modify-id"
                className={classes.Modify}
                as={Button}
                variant={Card.Header}
                eventKey="2"
              >
                <h2 className={classes.AccordionTitle}>Muokkaa organisaation tietoja</h2>
                <p aria-hidden="true" style={{margin:'0'}}>{modifyExpandedSymbol}</p>
              </Accordion.Toggle>
              <Accordion.Collapse aria-labelledby="accordion-1" id="modify-id" eventKey="2">
                <Card.Body className={classes.CardContainer}>
                  <Row style={{color:'black', textAlign:'left'}}>
                    <div className={classes.OrgFormContainer}>
                      <Form.Group style={{display:'grid'}}>
                        <Row className={classes.RowContainer}>
                          <div className={classes.Orgdata}>
                            <Form.Label htmlFor="organisation-name">Organisaation nimi</Form.Label>
                            <Form.Control id="organisation-name" type="text" onChange={this.handleValueChange('organization_name')} placeholder='Organisaation nimi' defaultValue={this.state.selectedOrganizationData.organization_name} />
                          </div>
                          <div className={classes.Orgdata}>
                            <Form.Label htmlFor="organisation-email">Organisaation sähköposti</Form.Label>
                            <Form.Control id="organisation-email" type="email" placeholder='Organisaation sähköposti' onChange={this.handleValueChange('email')} defaultValue={this.state.selectedOrganizationData.email}/>
                          </div>
                        </Row>
                        <Row className={classes.RowContainer}>
                          <div className={classes.Orgdata}>
                            <Form.Label htmlFor="organisation-phone">Organisaation puhelinnumero</Form.Label>
                            <Form.Control id="organisation-phone" type="text" placeholder='Organisaation puhelinnumero' onChange={this.handleValueChange('phone')} defaultValue={this.state.selectedOrganizationData.phone}/>
                          </div>
                          <div className={classes.Orgdata}>
                            <Form.Label htmlFor="organisation-web">Organisaation verkkosivut</Form.Label>
                            <Form.Control id="organisation-web" type="text" placeholder='Organisaation verkkosivut' onChange={this.handleValueChange('link')} defaultValue={this.state.selectedOrganizationData.link}/>
                          </div>
                        </Row>
                      </Form.Group>
                      <Form.Group style={{display:'grid'}}>
                        <Row className={classes.RowContainer}>
                          <div className={classes.Address}>
                            <Form.Label htmlFor="organisation-address">Katuosoite</Form.Label>
                            <Form.Control id="organisation-address" type="text" placeholder='Katuosoite' onChange={this.handleValueChange('address')} defaultValue={this.state.selectedOrganizationData.address}/>
                          </div>
                        </Row>
                        <Row className={classes.RowContainer}>
                          <div className={classes.Postalcode}>
                            <Form.Label htmlFor="organisation-postcode">Postinumero</Form.Label>
                            <Form.Control id="organisation-postcode" type="text" placeholder='Postinumero' onChange={this.handleValueChange('postcode')} defaultValue={this.state.selectedOrganizationData.postcode}/>
                          </div>
                          <div className={classes.City}>
                            <Form.Label htmlFor="organisation-city">Kaupunki</Form.Label>
                            <Form.Control id="organisation-city" type="text" placeholder='Kaupunki' onChange={this.handleValueChange('city_name')} defaultValue={this.state.selectedOrganizationData.city_name}/>
                          </div>
                        </Row>
                        <Row className={classes.RowContainer}>
                          <Button variant='success' className={classes.Button} onClick={this.handleButtonPress}>Tallenna muutokset</Button>
                        </Row>
                      </Form.Group>
                    </div>
                  </Row>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </div>
      </div>
    )
  }
}

export default OrganizationPage
