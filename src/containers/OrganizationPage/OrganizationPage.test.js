import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import OrganizationPage from './OrganizationPage';

it('OrganizationPage should not have basic accessibility issues', async () => {
  const { container } = render(<OrganizationPage />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
