import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import StatisticsPage from './StatisticsPage';

it('StatisticsPage should not have basic accessibility issues', async () => {
  const { container } = render(<StatisticsPage />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
