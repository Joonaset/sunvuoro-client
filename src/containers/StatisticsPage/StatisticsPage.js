import React, { Component } from 'react';
import axios from 'axios';
import { getDataCollectionRoute, getTaskRoute, getLocationsRoute, getOrganizationRoute } from '../../utils/Api';
import { Container, Row, Col } from 'react-bootstrap';
import  { Polar, Bar } from 'react-chartjs-2';
import Loader from '../../components/Loader';
import { getUser } from '../../utils/AuthUtils';

class StatisticsPage extends Component {
    state = {
        searchValuesRaw:[],
        taskValuesRaw: [],
        contactValuesRaw: [],
        organizations: [],
        colors: [
            '#35373F',
            '#84AA82',
            '#F3DFB5',
            '#E28B72',
            '#C04142',
            '#4A3D4D',
            '#EA4F4A',
            '#308E83',
            '#FADAA6',
            '#CCAF70'
        ]
    }

    async componentDidMount() {
        await axios.get(getOrganizationRoute()+getUser().userId).then(response => {
            this.setState({organizations: response.data.results});
        })

        getUser().isAdmin ? 
        await axios.get(getTaskRoute()).then(response => {
            this.setState({taskValuesRaw: response.data.results});
        }):
        await axios.get(getLocationsRoute()+this.state.organizations[0].location_id).then(response => {
            this.setState({taskValuesRaw: response.data.results});
        })

        getUser().isAdmin ?
        await axios.get(getDataCollectionRoute()+'searchValues').then(response => {
            this.setState({searchValuesRaw: response.data.results});
        }) : this.setState(this.state);
    }

    getAverageValues = () => {
        let averages = {
            social: 0.0,
            physical: 0.0,
            problems: 0.0,
        }

        let count = 0
        
        this.state.searchValuesRaw.forEach(val => {
            averages.physical += val.fysiikka_value;
            averages.social += val.sosiaalisuus_value;
            averages.problems += val.ajattelu_value;
            count += 1;
        })

        averages.social = averages.social / count;
        averages.physical = averages.physical / count;
        averages.problems = averages.problems / count;   

        return this.getDataset([averages.social.toFixed(2), averages.physical.toFixed(2), averages.problems.toFixed(2)], 
                                this.state.colors,
                                'Keskiarvohaut',
                                ['Sosiaalisuus', 'Fyysisyys', 'Ongelmanratkonta']);
    }

    getTopTasksOnClicks = () => {
        const tasks = [...this.state.taskValuesRaw];
        tasks.sort((a, b) => (a.taskClicks < b.taskClicks) ? 1 : -1);
        const taskCount = 5;
        let tasksToShow = [];
        let taskLabels = [];
        for(let i = 0; i < taskCount; i++) {
            tasksToShow.push(tasks[i].taskClicks);
            taskLabels.push(tasks[i].name);
        }
        return this.getDataset(tasksToShow, this.state.colors, '', taskLabels);
    }

    getDataset = (vals, cols, label, labels) => {
        return {
            datasets: [{
                data: [...vals],
                backgroundColor: [...cols],
                label: label
            }],
            labels: [
                ...labels
            ],
        };
    }

    getOptions = (xAxisVisible, yAxisVisible, maintainAspectRatio) => {
        return {
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [{
                        display: xAxisVisible,
                        ticks: {
                            min:0
                        }
                    }],
                    yAxes: [{
                        display: yAxisVisible,
                        ticks: {
                            min:0
                        }
                    }],
                },
                maintainAspectRatio: maintainAspectRatio
        }
    }

    render() {

        const polarChart = this.state.searchValuesRaw.length > 0 
        ? <Polar data={this.getAverageValues()} width={250} height={250} options={this.getOptions(false, false, false)}/> 
        : <Loader size={'sm'} />
        const polarLayout = <Col style={{backgroundColor:'#fff', padding:'3rem'}}><Row style={{justifyContent:'center'}}>Hakujen keskiarvot</Row><Row>{polarChart}</Row></Col>
        const averageRow = getUser().isAdmin ? polarLayout : null;

        const barChart = this.state.taskValuesRaw.length > 0 
        ? <Bar data={this.getTopTasksOnClicks()} width={250} height={250} responsive={true} options={this.getOptions(false, true, false)}/> 
        : <Loader size={'sm'}/>
        const mostClickedRow = <Col style={{backgroundColor:'#fff', padding:'3rem'}}><Row style={{justifyContent:'center'}}>Klikatuimmat tehtävät</Row><Row>{barChart}</Row></Col>


        return(
        <Container>
            <Row style={{ backgroundColor: 'white', display: 'flex', justifyContent: 'center', paddingTop: '2rem' }}>
                <h1>Tilastot</h1>
            </Row>
            <Row style={{ backgroundColor: 'white', marginBottom: '3rem' }} md={2}>
                {averageRow}
                {mostClickedRow}
            </Row>
        </Container>
        );
    }
}

export default StatisticsPage;