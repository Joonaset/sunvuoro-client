import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import { Row } from 'react-bootstrap';
import Button from '@material-ui/core/Button';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import classes from './RegisterPage.module.css';
import { getApplicationRoute } from '../../utils/Api';
import Popup from '../../components/Popup/Popup';

const RegisterPage = () => {
  const [organizationFormData, setOrganizationFormData] = useState({
    organization_name: '',
    organization_email: '',
    organization_phone: '',
    organization_link: '',
    organization_city: '',
    organization_address: '',
    organization_postcode: '',
    free_text: '',
  });

  const [alert, setAlert] = useState({
    success: false,
    alertType: '',
    alertMessage: '',
    showAlert: false,
    closedAlert: false
  });
  const [errorMessages, setErrorMessages] = useState(null);

  const popupRef = useRef(null);
  const errorRef = useRef(null);
  const pageRef = useRef(null);

  const setAlertMessage = (alertType) => {
    switch (alertType) {
      case 'success':
        return { message: 'Rekisteröinti onnistui! Hakemuksenne käsitellään pian ja vastaus lähetetään sähköpostiinne!', success: true};
      case 'error':
        return { message: 'Rekisteröinti epäonnistui!', success: false };
      default:
        return 'alertMessage not found';
    }
  };

  const togglePopup = (alertType) => {
    const newAlert = setAlertMessage(alertType);
    setAlert({ ...alert, showAlert: !alert.showAlert, alertType, alertMessage: newAlert.message, success: newAlert.success, closedAlert: alert.showAlert });
  };

  const sendOrganization = () => {
    const payload = organizationFormData;
    axios.post(getApplicationRoute(), payload).then(response => console.log('result', response));
    togglePopup('success');
  };

  const handleValueChange = name => event => {
    const data = { ...organizationFormData };
    data[`${name}`] = event.target.value;
    setOrganizationFormData(data);
  };

  const applyErrorMessages = errors => {
    const messages = errors
      .map(({ props, getErrorMessage }) => ({ label: props.label, getErrorMessage }));
    setErrorMessages(messages);
  };

  useEffect(() => {
    if (alert.showAlert && popupRef.current) {
      popupRef.current.focus();
    }

    if (alert.closedAlert && pageRef && pageRef.current) {
      pageRef.current.focus();
    }
  }, [alert]);

  useEffect(() => {
    if (errorMessages && errorRef.current) {
      errorRef.current.focus();
    }
  }, [errorMessages]);

  const style = {
    width: '100%',
  };

  const data = organizationFormData;

  return (
    <div className={classes.Container}>
      {alert.showAlert
        && <Popup
          role="dialog"
          ref={popupRef}
          className={alert.success ? 'popup_inner_success' : 'popup_inner_error'}
          text={alert.alertMessage}
          closePopup={() => togglePopup('success')}
        />}
      <div style={{ display: alert.showAlert ? 'block' : 'none' }} className="DialogOverlay" tabIndex="-1" />
      <div
        aria-labelledby="error-label"
        tabIndex="-1"
        ref={errorRef}
        id="error-messages"
        className={classes.ErrorMessages}
        style={{ display: errorMessages ? 'block' : 'none' }}
      >
        <h1 id="error-label">Lomakeessa on virheitä</h1>
        <ul>
          {errorMessages && errorMessages
            .map((error, i) => <li key={i}>
              {error.label}
              :
              {error.getErrorMessage()}
          </li>)}
        </ul>
      </div>
      <ValidatorForm
        onSubmit={sendOrganization}
        onError={errors => applyErrorMessages(errors)}
      >
        <h1 tabIndex="-1" ref={pageRef} className={classes.OrgContactTitle}>Organisaation yhteystiedot:</h1>
        <Row>
          <div className={classes.OrgFormContainer}>
            <label htmlFor="organization-name" className={classes.OrganizationLabel}>Organisaation nimi</label>
            <TextValidator
              id="organization-name"
              aria-describedby="contact-info-description"
              style={style}
              onChange={handleValueChange('organization_name')}
              name="organization_name"
              value={data.organization_name}
              validators={['required']}
              errorMessages={['Vaadittu']}
            />
            <div id="contact-info-description" className={classes.Label}>
            Organisaation nimi näkyy tehtävien hakijoille tehtävän tarjoavana tahona. Annathan siis nimen sellaisena,
            kuin se löytyy muista lähteistä (esim kotisivut).
            </div>
          </div>
          <div className={classes.OrgFormContainer}>
            <label htmlFor="organization-email" className={classes.OrganizationLabel}>Organisaation sähköposti</label>
            <TextValidator
              id="organization-email"
              aria-describedby="email-description"
              style={style}
              onChange={handleValueChange('organization_email')}
              name="organization_email"
              value={data.organization_email}
              validators={['required', 'isEmail']}
              errorMessages={['Vaadittu', 'Tarkista sähköposti']}
            />
            <div id="email-description" className={classes.Label}>
            Tähän sähköpostiosoitteeseen tehtävien hakijat ottavat yhteyttä.
            <p className={classes.LabelALERT}> HUOM! Tietosuojalaista johtuen annathan organisaation yleisen sähköpostiosoitteen, jossa
            ei näy henkilöön yhdistettäviä tietoja! </p>
            </div>
          </div>
        </Row>
        <Row>
          <div className={classes.OrgFormContainer}>
            <label htmlFor="organization-phone" className={classes.OrganizationLabel}>Organisaation puhelinnumero</label>
            <TextValidator
              id="organization-phone"
              aria-describedby="phone-description"
              style={style}
              onChange={handleValueChange('organization_phone')}
              name="organization_phone"
              value={data.organization_phone}
              validators={['required', 'isNumber', 'maxStringLength:10']}
              errorMessages={['Vaadittu', 'Tarkista puhelinnumero (Syötä ilman suuntanumeroa)', 'Liian pitkä puhelinnumero']}
            />
            <div id="phone-description" className={classes.Label}>
            Puhelinnumero näkyy tehtäviä etsiville tehtävän yhteystietokentässä. Tehtävistä kiinnostuneet ottavat siis mahdollisesti
            yhteyttä tähän puhelinnumeroon.
            </div>
          </div>
          <div className={classes.OrgFormContainer}>
            <label htmlFor="organization-web" className={classes.OrganizationLabel}>Organisaation verkkosivut</label>
            <TextValidator
              id="organization-web"
              aria-describedby="web-description"
              style={style}
              onChange={handleValueChange('organization_link')}
              name="organization_link"
              value={data.organization_link}
              validators={['required']}
              errorMessages={['Vaadittu']}
            />
            <div id="web-description" className={classes.Label}>
            Verkkosivut näkyvät tehtäväkentän yhteystiedoissa. Annathan siis sen verkkosivun osoitteen, josta tehtävästä kiinnostunut
            löytää oikeat ja ajankohtaiset tiedot organisaatiostanne.
            </div>
          </div>
        </Row>
        <h1 className={classes.OrgContactTitle}>Organisaation osoitetiedot:</h1>
        <Row>
          <div className={classes.OrgFormContainer}>
            <label htmlFor="organization-address" className={classes.OrganizationLabel}>Katuosoite</label>
            <TextValidator
              id="organization-address"
              aria-describedby="address-description"
              style={style}
              onChange={handleValueChange('organization_address')}
              name="organization_address"
              value={data.organization_address}
              validators={['required']}
              errorMessages={['Vaadittu']}
            />
            <div id="address-description" className={classes.Label}>
            Katuosoite näytetään hakijalle sekä tekstimuodossa, että karttanäkymässä. Varmistathan, että osoitetiedot ovat oikein,
            jotta karttanäkymä kohdistuu oikein ja tehtävistä kiinnostuneille välitetään oikea sijainti.
            </div>
          </div>
        </Row>
        <Row>
          <div className={classes.OrgFormContainer2}>
            <div className={classes.PostCodeContainer}>
              <label htmlFor="organization-postcode" className={classes.OrganizationLabel}>Postinumero</label>
              <TextValidator
                id="organization-postcode"
                aria-describedby="postcode-description"
                style={style}
                onChange={handleValueChange('organization_postcode')}
                name="organization_postcode"
                value={data.organization_postcode}
                validators={['required', 'isNumber', 'maxStringLength:5']}
                errorMessages={['Vaadittu', 'Tarkista postinumero', 'Postinumero on liian pitkä']}
              />
            </div>
            <div id="postcode-description" className={classes.PostCityContainer}>
              <label htmlFor="organization-city" className={classes.OrganizationLabel}>Kaupunki</label>
              <TextValidator
                id="organization-city"
                style={style}
                onChange={handleValueChange('organization_city')}
                name="organization_city"
                value={data.organization_city}
                validators={['required']}
                errorMessages={['Vaadittu']}
              />
            </div>
          </div>
        </Row>
        <h1 className={classes.OrgContactTitle}>Vapaa tekstikenttä mahdollisia lisätietoja varten:</h1>
        <Row>
          <div className={classes.OrgFormContainer}>
            <label htmlFor="organization-free-text" className={classes.OrganizationLabel}>Vapaa teksti</label>
            <TextValidator
              id="organization-free-text"
              style={style}
              className={classes.Orgdata}
              onChange={handleValueChange('free_text')}
              name="free_text"
              value={data.free_text}
            />
          </div>
        </Row>
        <Row>
          <div className={classes.OrgFormContainer3}>
            <Button style={style} type="submit">Lähetä hakemus</Button>
          </div>
        </Row>
      </ValidatorForm>
    </div>
  );
};

export default RegisterPage;
