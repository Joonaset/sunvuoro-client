import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import RegisterPage from './RegisterPage';

it('RegisterPage should not have basic accessibility issues', async () => {
  const { container } = render(<RegisterPage />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
