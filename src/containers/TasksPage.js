import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Task from '../components/TaskCard/Task';
import TaskModal from '../components/TaskCard/TaskModal';
import axios from 'axios';
import { getTaskRoute, getCitiesRoute, getDataCollectionRoute, getWorkTimesRoute} from '../utils/Api';
import { getTaskData } from '../utils/DataUtils';
import Loader from '../components/Loader';
import classes from '../containers/TasksPage.module.css';
import addImage from '../../src/images/add_new_task.png';
import { Multiselect } from 'multiselect-react-dropdown';
import Popup from '../components/Popup/Popup'

class TasksPage extends Component {
    state = {
      modalOpen: false,
      tasks: [{
        task_id: 0,
        name: '',
        description: '',
        fysiikka_value: 3,
        sosiaalisuus_value: 3,
        ajattelu_value: 3,
        image: '',
        location_id: 0,
        city_name: '',
        url: '',
        longitude: 0.0,
        lat: 0.0,

      }],
      cities: [{
        city_name: '',
        city_id: 0
      }],
      modalData: {
        name:'',
        task_id: 0,
        description: '',
        image: '',
        imageAlt: '',
        longitude: 0.0,
        latitude: 0.0, 
        address: '',
        postcode: '',
        task_when: '',
        organization_name: '',
        link: '',
        phone: '',
        email: '',
        applicationSender: '',
        applicationEmail: '',
        applicationBody: '',
        fysiikka: 0,
        sosiaalisuus: 0,
        ajattelu: 0
      },
      loading: true,
      selectedCityTasks: [{}],
      selectedWorktimeTasks: [{}],
      defaultSelected: [{}],
      defaultSelectedTimes: [{}],
      workTimes: [{
        worktime_id: 0,
        worktime: ''
      }],
      tasksWithWorkTimes: [{
        task_id: 0,
        worktime_id: 0
      }],
      alert: {
        showAlert: false,
        success:false,
        alertType: "",
        alertMessage: ""
      },
      closedPopup: false,
      modalTitleRef: null,
      pageRef: null
    }

    setModalTitleRef = element => {
      this.modalTitleRef = element;
    }

    setPageRef = element => {
      this.pageRef = element;
    }

    focusTextInput = () => {
      if (this.modalTitleRef) this.modalTitleRef.focus();
    };

    focusOnPage = () => {
      console.log('focusOnPage')
      if (this.pageRef) this.pageRef.focus();
    };

    togglePopup = (alertType) => {
      const data = this.state.alert
      data.alertType = alertType;
      data.showAlert = !data.showAlert
      this.setState({
        alert: data
      })
      this.setAlertMessage(alertType)
      console.log(this.state.alert)
      if (!data.showAlert) {
        this.setState({ closedPopup: true })
      } else {
        this.setState({ closedPopup: false })
      }
    }

    setAlertMessage = (alertType) => {
      const data = this.state.alert
      switch(alertType) {
        case "success":
          data.success = true;
          data.alertMessage = `Viesti lähetetty onnistuneesti, olemme teihin yhteydessä mahdollisimman pian!`
          break;
        case "error":
          data.success = false;
          data.alertMessage = "Viestin lähetys epäonnistui!"
          break;
        default:
          data.alertMessage = "alertMessage not found"
      }
      this.setState({
        alert:data
      })
    }

    async componentDidMount() {
      this.setState({loading: true});
      await axios.get(getTaskRoute()).then(response => {
        this.setState({tasks: response.data.results });
        this.setState({selectedCityTasks: response.data.results});
        this.setState({selectedWorktimeTasks: response.data.results})
      });
      await axios.get(getCitiesRoute()).then(response => {
        const cities = response.data.results.map(city => {
          return {
            city_name: city.city_name,
            city_id: city.city_id
          }
        })
        
        this.setState({cities: cities})
      })

      await axios.get(getWorkTimesRoute()).then(response => {
        const workTimes = response.data.results.map(worktime => {
          return {
            worktime_id: worktime.worktime_id,
            worktime: worktime.worktime
          }
        })
        this.setState({workTimes: workTimes})
      })

      await axios.get(getTaskRoute()+'taskWorktimes').then(response => {
        this.setState({tasksWithWorkTimes: response.data.results});
      })

      const defaultVals = this.state.cities.map(city => {
        return {name: city.city_name}
      })

      const defaultTimes = this.state.workTimes.map(time => {
        return { name: time.worktime, id: time.worktime_id }
      })

      this.setState({defaultSelected: defaultVals})
      this.setState({defaultSelectedTimes: defaultTimes });
      this.setState({loading:false})
    }

    componentDidUpdate(prevProps, prevState) {
      if ((prevState.modalOpen !== this.state.modalOpen) && this.state.modalOpen) {
        this.focusTextInput()
      }
      if (this.state.closedPopup) {
        this.focusOnPage()
      }
    }

    openModal = (key) => {
      const task = getTaskData(key, this.state.tasks);
      this.setState({modalData: task, modalOpen: true});
      task.applicationBody = `Hei, \nolen kiinnostunut tehtävästänne ` + task.name
      axios.put(getDataCollectionRoute()+`taskClicks/${task.task_id}`)
      .then(result => console.log("result:",result))
    }

    handleCityChange = event => {
      const data = [...this.state.tasks]
      let selectedTasks = [];
      data.forEach(task => {
        event.forEach(city => {
          if(city.name === task.city_name) {
            selectedTasks.push(task);
          }
        })
      })

      this.setState({selectedCityTasks: selectedTasks});
    }

    handleWorktimeChange = event => {
      const data = [...this.state.tasks]
      const ids = [...this.state.tasksWithWorkTimes];

      let selectedTasks = [];
      ids.forEach(id => {
        event.forEach(worktime => {
          if(id.worktime_id === worktime.id) {
            data.forEach(task => {
              if(task.task_id === id.task_id) {
                selectedTasks.push(task);
              }
            })
          }
        })
      })
      selectedTasks.filter((item, index) => selectedTasks.indexOf(item) === index);
      this.setState({selectedWorktimeTasks: selectedTasks});
    }

    closeSuggestionModal = () => {
      //close modal after sending email
      //tää ei vielä lähetä sähköpostia sitten mihinkään
      this.setState({modalOpen: false})
    }
    sendEmail = () => {
      const applicationData = {
        name: this.state.modalData.applicationSender,
        senderEmail: this.state.modalData.applicationEmail,
        bodyText: this.state.modalData.applicationBody,
        orgEmail: this.state.modalData.email,
        taskName: this.state.modalData.name
      }
      axios.post(getTaskRoute()+"application",applicationData)
      .then(result => console.log("result:",result))
      axios.put(getDataCollectionRoute()+`contactClicks/${this.state.modalData.task_id}`)
      .then(result => console.log("result:",result))
      this.togglePopup("success")
      //close modal after sending email
      this.setState({modalOpen: false})
    }

    handleValueChange = name => event => {
      const data = {...this.state.modalData}
      data[`${name}`] = event.target.value;
      this.setState({
        modalData: data
      })
    }

    getWorktimeNames = taskId => {
      let workTimesString = ""
      if(this.state.tasksWithWorkTimes != null) {
        this.state.tasksWithWorkTimes.map(task => {
          this.state.workTimes.map(worktime => {
            if (task.task_id === taskId && task.worktime_id == worktime.worktime_id) {
              workTimesString += worktime.worktime + ", "
            }
          })
        })
        return workTimesString.substring(0,workTimesString.length-2);
      }
    }

    render() {
      let tasks = [];
      let counter = 1;
      let loader = <Loader size=""/>
      if(!this.state.loading){
        // every 10th task view is for proposing new task
        if (counter%10===0) {
          counter++;
          // HUOM! key ei ole uniikki, pitäisi ratkaista jotenkin
          tasks.push(<Task key=""
            image={addImage}
            name='Ehdota tehtävää'
            city='Etkö löytänyt mieleistäsi tehtävää? Ehdota tästä!'
            address=''
            postcode=''
            task_when=''
            organization_name=''
            imageAlt="Ehdota tehtävää"
            onClick={() => this.openModal("add")}
          />
          )
        }

        this.state.selectedCityTasks.forEach(task => {
          counter++;
          if(this.state.selectedWorktimeTasks.includes(task)) {
            tasks.push(<Task key={task.task_id}
              image={task.image}
              name={task.name}
              city={task.city_name}
              address={task.address}
              postcode={task.postcode}
              task_when={task.task_when}
              organization_name={task.organization_name}
              link={task.link}
              phone={task.phone}
              email={task.email}
              fysiikkaValue={task.fysiikka_value}
              sosiaalisuusValue={task.sosiaalisuus_value}
              ajatteluValue={task.ajattelu_value}
              imageAlt={task.image_alt || `Työtä '${task.name}' esittävä kuva`}
              onClick={() => this.openModal(task.task_id)}/>)
          }
        })
        //also the last task is for proposing new task
        tasks.push(<Task key=""
          image={addImage}
          name='Ehdota tehtävää'
          city='Etkö löytänyt mieleistäsi tehtävää? Ehdota tästä!'
          address=''
          postcode=''
          task_when=''
          organization_name=''
          fysiikkaValue=""
          sosiaalisuusValue=""
          ajatteluValue=""
          onClick={() => this.openModal("add")}/>
        )
        loader = null;
      }

      const citiesDropdown = this.state.cities.map(city => {
        return { name: city.city_name }
      })
      
      const worktimesDropdown = this.state.workTimes.map(worktime => {
        return { 
          name: worktime.worktime, 
          id: worktime.worktime_id
        }
      })
      this.getWorktimeNames()
      return(
        <Container style={{minHeight:'80vh', maxWidth:'90vw', margin:'5vw'}}>
          {this.state.alert.showAlert
            && <Popup
              role="dialog"
              className={this.state.alert.success ? "popup_inner_success" : "popup_inner_error"}
              text={this.state.alert.alertMessage}
              closePopup={this.togglePopup.bind(this)}
              />}
          <div style={{ display: this.state.alert.showAlert ? 'block' : 'none' }} className="DialogOverlay" tabIndex="-1" />
          <div style={{ background:'rgba(235, 226, 226, 0.79)', textAlign:'center', display:'flex', flexDirection:'column', margin:'2px'}}>
            <h1 tabIndex="-1" ref={this.setPageRef} style={{textAlign:'center', marginTop:'8px'}}>Kaikki tehtävät</h1>
            <Row className={classes.FilterTop}></Row>
            <Row className={classes.FilterContainer}>
              <Col className={classes.SingleFilter}>
              <div aria-labelledby="sijainti-label">
                <h2 className={classes.SelectorHeader} id="sijainti-label">Kaupungit</h2>
                <label htmlFor="select-location" aria-label="Valitse sijainti. Kirjoita rajataksesi hakua." style={{ marginBottom: '0.1rem' }}>
                  Valitse sijainti
                </label>
                  <Multiselect
                    id="select-location"
                    placeholder=""
                    emptyRecordMsg=""
                    options={citiesDropdown}
                    displayValue="name" 
                    onSelect={this.handleCityChange}
                    onRemove={this.handleCityChange} 
                    selectedValues={this.state.defaultSelected}
                    placeholder="Valitse tai kirjoita"
                    closeOnSelect={false}
                    showCheckbox={false}
                    closeIcon="close"
                    avoidHighlightFirstOption={true}
                    />
                </div>
              </Col>
              <Col className={classes.SingleFilter}>
              <div aria-labelledby="ajankohdat-label">
                <h2 className={classes.SelectorHeader} id="ajankohdat-label">Tehtävän ajankohdat</h2>
                <label htmlFor="select-time-of-day" aria-label="Valitse ajankohta. Kirjoita rajataksesi hakua." style={{ marginBottom: '0.1rem' }}>
                  Valitse tehtävän ajankohdat
                </label>
                <Multiselect
                  id="select-time-of-day"
                  placeholder=""
                  emptyRecordMsg=""
                  options={worktimesDropdown} 
                  displayValue="name"
                  onSelect={this.handleWorktimeChange}
                  onRemove={this.handleWorktimeChange}
                  selectedValues={this.state.defaultSelectedTimes}
                  placeholder="Valitse tai kirjoita"
                  closeOnSelect={false}
                  showCheckbox={false}
                  closeIcon="close"
                  avoidHighlightFirstOption={true}
                  />
                </div>
              </Col>
            </Row>
            <Row className={classes.FilterBottom}></Row>
            <TaskModal modalState={this.state.modalOpen}
                      name={this.state.modalData.name}
                      description={this.state.modalData.description}
                      image={this.state.modalData.image}
                      imageAlt={this.state.modalData.imageAlt || `Työtä '${this.state.modalData.name}' esittävä kuva`}
                      city={this.state.modalData.city_name}
                      longitude={this.state.modalData.longitude}
                      latitude={this.state.modalData.latitude}
                      address={this.state.modalData.address}
                      postcode={this.state.modalData.postcode}
                      task_when={this.state.modalData.task_when}
                      organization_name={this.state.modalData.organization_name}
                      email={this.state.modalData.email}
                      link={this.state.modalData.link}
                      phone={this.state.modalData.phone}
                      workTimes={this.getWorktimeNames(this.state.modalData.task_id)}
                      closeModal={() => this.setState({modalOpen: false})}
                      sendEmail={() => this.sendEmail()}
                      fysiikka_value={this.state.modalData.fysiikka_value}
                      sosiaalisuus_value={this.state.modalData.sosiaalisuus_value}
                      ajattelu_value={this.state.modalData.ajattelu_value}
                      closeSuggestionModal={() => this.closeSuggestionModal()}
                      handleValueChange={this.handleValueChange}
                      titleRef={this.setModalTitleRef} />
            <Row className={classes.Loader}>
              {loader}
            </Row>
            <Row>
              {tasks}
            </Row>
          </div>
        </Container>
      );
    }
  }

  export default TasksPage;
