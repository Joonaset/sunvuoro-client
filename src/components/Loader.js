import React from 'react';
import Spinner from 'react-bootstrap/Spinner';

const loader = ({ size }) => {
  return (
    <div>
      <Spinner animation="border" role="status" size={size} variant="secondary">
        <span className="sr-only">Loading...</span>
      </Spinner>
    </div>
  );
};

export default loader;
