import React from 'react';
import { Link } from 'react-router-dom';
import {
  Button, Container, Row, Col
} from 'react-bootstrap';
import classes from './Main.module.css';
import VVLogo from '../../images/VV_Logo.png';
import EULogo from '../../images/EU_Logo.png';
import InfoBubble from '../../images/info_bubble.png';
import CookieConsent, { Cookies } from "react-cookie-consent";


const Main = () => {
  const smallFakeButton = <span className={classes.SmallButton}>Löydä sun juttu! ►</span>;
  const fakeKaikkiTehtavat = <span className={classes.KaikkiTehtavatInstruction}>Kaikki tehtävät</span>;

  return (
    <div className={classes.PageContainer}>
      <div className={classes.Flags}>
        <img className={classes.VipuVoimaaFlag} alt="Vipuvoimaa-logo" src={VVLogo} />
        <img className={classes.EuroopanUnioniFlag} alt="EU-logo" src={EULogo} />
      </div>
      <Container className={classes.Container}>
        <Row className={classes.rowContainer}>
          <h1 className={classes.Header1}>Nyt on Sun vuoro!</h1>
          <Col className={classes.Description1}>
            <Row className={classes.AfterDescription} />
            <h2 className={classes.Header2}>Mitä tekis, missä tekis, miten tekis?</h2>
            <Row className={classes.AfterDescription} />
          </Col>
          <Col className={classes.ButtonContainer}><a tabIndex="-1" href="/quiz"><Button className={classes.Button1}>Löydä sun juttu! ►</Button></a></Col>
        </Row>
        <Row className={classes.Tooltip}>
          <img className={classes.TooltipImage} alt="" src={InfoBubble} />
          <ul className={classes.TooltipText}>
            <li>
              <span aria-hidden="true">► </span>
              Onko sinulla aikaa, jonka voisit antaa yhteiselle tekemiselle? Etsitkö paikkaa, jossa suorittaa työkokeilua, työharjoittelua tai jossa osallistua vapaaehtoistoimintaan?
            </li>
            <li>
              <span aria-hidden="true">► </span>
              Pohdi yksin tai yhdessä jonkun kanssa minkälainen tekeminen voisi hyödyttää sinua tällä hetkellä
            </li>
            <li>
              <span aria-hidden="true">► </span>
              Klikkaa {smallFakeButton} -painiketta ja hae tehtäviä
            </li>
            <li><span aria-hidden="true">► </span>
              Voit myös selata kaikkia tehtäviä {fakeKaikkiTehtavat} -välilehdeltä
            </li>
          </ul>
        </Row>
        <CookieConsent
        buttonText="OK"
        style={{ background: "#2B373B" }}>
          Käytämme evästeitä parantaaksemme sivuston käyttökokemusta. Hyväksyt evästeiden käytön jatkamalla sivun selaamista.
        </CookieConsent>
      </Container>
    </div>
  );
};

export default Main;
