import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Main from './Main';

it('Main should not have basic accessibility issues', async () => {
  const { container } = render(<Main />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
