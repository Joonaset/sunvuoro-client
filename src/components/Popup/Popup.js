import React, { useEffect, useCallback } from 'react';
import FocusTrap from 'focus-trap-react';
import './Popup.css';

const popup = React.forwardRef((props, ref) => {
  const escFunction = useCallback((event) => {
    if (event.keyCode === 27) {
      props.closePopup();
    }
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false);
    return () => {
      document.removeEventListener('keydown', escFunction, false);
    };
  }, []);

  return (
    <div className="popup">
      <FocusTrap>
        <div className={props.className}>
          <p id="popup-text">{props.text}</p>
          <button aria-describedby="popup-text" ref={ref} type="button" style={{ fontSize: '1rem' }} onClick={props.closePopup}>Sulje</button>
        </div>
      </FocusTrap>
    </div>
  );
});

export default popup;
