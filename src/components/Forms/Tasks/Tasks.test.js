import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import AdminInstructions from './AdminInstructions';
import DescriptionFields from './TaskDescriptionFields';
import FileUploader from './TaskFileUploader';
import Submitter from './TaskSubmitter';
import TaskValueSelector from './TaskValueSelector';
import TaskValueSelectors from './TaskValueSelectors';

describe('Task components', () => {
  it('AdminInstructions should not have basic accessibility issues', async () => {
    const { container } = render(<AdminInstructions />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('TaskDescriptionFields should not have basic accessibility issues', async () => {
    const { container } = render(<DescriptionFields />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('FileUploader should not have basic accessibility issues', async () => {
    const { container } = render(<FileUploader />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('TaskSubmitter should not have basic accessibility issues', async () => {
    const { container } = render(<Submitter buttonText="button text" />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('TaskValueSelector should not have basic accessibility issues', async () => {
    const { container } = render(<TaskValueSelector />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  it('TaskValueSelectors should not have basic accessibility issues', async () => {
    const { container } = render(<TaskValueSelectors />);
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
