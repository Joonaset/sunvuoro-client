import React from 'react';
import Form from 'react-bootstrap/Form';
import TaskContext from '../../../utils/Context/TaskContext';
import ValueSelector from './TaskValueSelector';

import classes from '../../../containers/AdminPage/AdminPage.module.css';
import classes2 from './Tasks.module.css';

const taskValueSelectors = () => {
  const thinkingDescription = `
    PULMANRATKAISU tarkoittaa oppimista, opitun tiedon soveltamista, ajattelemista, ongelmien ratkaisemista ja päätösten tekemistä
  `;
  const sociabilityDescription = `
    SOSIAALISUUS tarkoittaa henkilöiden välisiä, asiayhteyteen ja sosiaaliseen tilanteeseen sopivia perustavia ja monimutkaisia
    vuorovaikutussuhteita ja niiden edellyttämiä toimia ja tehtäviä
  `;
  const physicalityDescription = `
    FYSIIKKA tarkoittaa liikkumista kehon asentoa tai sijaintia muutettaessa tai siirryttäessä paikasta toiseen, kannettaessa,
    liikutettaessa tai käsiteltäessä esineitä, käveltäessä, juostaessa, kiivettäessä tai käytettäessä erilaisia kulkuneuvoja
  `;
  return (
    <TaskContext.Consumer>
      {context =>
        <div className={classes.Bar} style={{ display: 'flex' }}>
          <div style={{ display: 'flex', flex: '1', flexDirection: 'column', padding: '2rem' }}>
            <div className={classes2.TaskValueRow}>
              <ValueSelector
                selectorClass={classes2.TaskValueSelector}
                screenReader={thinkingDescription}
                title="PULMANRATKAISU"
                id="ajattelu"
                val={context.values.ajattelu}
              />
              <p aria-hidden="true" className={classes2.TaskValueDescription}>{thinkingDescription}</p>
            </div>
            <div className={classes2.TaskValueRow}>
              <ValueSelector
                selectorClass={classes2.TaskValueSelector}
                screenReader={sociabilityDescription}
                title="SOSIAALISUUS"
                id="sosiaalisuus"
                val={context.values.sosiaalisuus}
              />
              <p aria-hidden="true" className={classes2.TaskValueDescription}>{sociabilityDescription}</p>
            </div>
            <div className={classes2.TaskValueRow}>
              <ValueSelector
                selectorClass={classes2.TaskValueSelector}
                screenReader={physicalityDescription}
                title="FYSIIKKA"
                id="fysiikka"
                val={context.values.fysiikka}
              />
              <p aria-hidden="true" className={classes2.TaskValueDescription}>{physicalityDescription}</p>
            </div>
          </div>
        </div>}
    </TaskContext.Consumer>);
};

export default taskValueSelectors;
