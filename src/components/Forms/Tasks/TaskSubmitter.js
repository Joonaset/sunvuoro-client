import React from 'react';
import Button from 'react-bootstrap/Button';
import TaskContext from '../../../utils/Context/TaskContext';
import Loader from '../../Loader';

import classes from '../../../containers/AdminPage/AdminPage.module.css';

const Submitter = React.forwardRef((props, ref) => {
  return (
    <TaskContext.Consumer>
      {context =>
        <Button
          ref={ref}
          onClick={() => context.buttonPressedHandler(props.type)}
          variant={props.variant}
          className={props.buttonClass ? props.buttonClass : classes.Button}
        >
          {context.loading ? <Loader size="sm" /> : props.buttonText}
        </Button>}
    </TaskContext.Consumer>
  );
});

export default Submitter;
