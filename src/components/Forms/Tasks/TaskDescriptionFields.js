import React from 'react';
import Form from 'react-bootstrap/Form';
import TaskContext from '../../../utils/Context/TaskContext';
import classes from './Tasks.module.css';

const DescriptionFields = React.forwardRef((props, ref) => {
  return (
    <TaskContext.Consumer>
      {context =>
        <div>
          <p>{context.header}</p>
          <div className={classes.DescriptionFieldsContainer}>
            <Form.Group className={classes.TaskName} controlId="task-name">
              <Form.Label className={classes.Labels}>Tehtävän nimi</Form.Label>
              <Form.Control style={{ border: '1px solid grey', borderRadius:'5px'}} ref={ref} type="text" placeholder="Tehtävän nimi" defaultValue={props.defaultName} onChange={context.handleValueChange('task', 'taskName', context.data)} />
            </Form.Group>
            <Form.Group className={classes.TaskLocation} controlId="task-location">
              <Form.Label className={classes.Labels}>Tehtävän sijainti</Form.Label>
              <Form.Control as="select" onChange={context.handleChange}>
                {context.location}
              </Form.Control>
            </Form.Group>
          </div>
          <Form.Group controlId="task-description">
            <Form.Label className={classes.Labels}>Tehtävän kuvaus</Form.Label>
            <Form.Control style={{ border: '1px solid grey', borderRadius:'5px', width: '80%'}} as="textarea" rows="3" placeholder="Tehtävän kuvaus" defaultValue={props.defaultDescription} onChange={context.handleValueChange('task', 'taskDescription', context.data)} />
          </Form.Group>
        </div>}
    </TaskContext.Consumer>
  );
});

export default DescriptionFields;
