import React from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import Button from 'react-bootstrap/Button';
import classes from '../../../containers/AdminPage/AdminPage.module.css';

const admininstructions = () => (
  <div>
    <br />
    <div className={classes.InstTopContainer}>
      <h1 id="top" className={classes.InstHeader}>
        Ohjeet hallintapaneelin käyttöön
      </h1>
      <div className={classes.InstInfo}>
        <p>
          Aloita valitsemalla haluttu toiminto. Uuden tehtävän lisäys tapahtuu vihreästä alavalikosta &quot;Uusi tehtävä&quot; jos
          olemassa olevan tehtävän muokkaus keltaisesta
          alavalikosta &quot;Muokkaa tehtävää&quot;. Mikäli haluat poistaa tehtävän, valitse punaisella taustalla merkattu &quot;Poista tehtävä&quot;.
        </p>
        <h2 className={classes.SecondaryTitle}>Sisällys:</h2>
        <ol>
          <li> <a href="#add"> Uuden tehtävän lisäys </a> </li>
          <li> <a href="#mod"> Olemassa olevan tehtävän muokkaus </a> </li>
          <li> <a href="#del"> Tehtävän poisto </a> </li>
        </ol>
        <div className={classes.InstHrLine} />
      </div>
    </div>
    <div style={{ paddingTop: '1rem', border: '1rem solid rgb(203, 245, 164)' }}>
      <h2 className={classes.InstTitle} id="add"> 1. Uuden tehtävän lisäys </h2>
      <div className={classes.Text}>
        <p>
          Valitse &quot;Uusi tehtävä&quot; vihreällä pohjalla klikkaamalla sitä. Alle aukeaa lomake, jonka kautta uusi tehtävä lisätään palveluun.
          Lomakkeen kaikki kentät ovat pakollisia, joten ennen tallennusta, tarkastathan että kaikki kentät ovat täytettynä ja sisältävät
          oikean tiedon. Uuden tehtävän lisäykseen vaaditaan seuraavat tiedot (järjestyksessä ylhäältä alas):
        </p>
        <h3 className={classes.InstSubTitle}>1.1 Tehtävän nimi: </h3>
        <p>
          Tehtävän nimi, joka näkyy myös tehtävän otsikkona. Esimerkiksi &quot;kahvilan vapaaehtoinen&quot;, &quot;leipuri&quot;, jne.
          Pidäthän tehtävän nimen mahdollisen lyhyenä ja ytimekkäänä.
        </p>
        <h3 className={classes.InstSubTitle}>1.2 Tehtävän paikka: </h3>
        <p>
          Nimen jälkeen löydät tehtävän sijainnin paikan, joka on automaattisesti oma organisaatiosi, eikä se siis ole muutettavissa. Mikäli kuitenkin huomaat,
          että organisaatio on väärä, niin päivitä tietosi
          <a href="/organizations"> Organisaatioiden hallinta -sivulta.</a>
        </p>
        <h3 className={classes.InstSubTitle}>1.3 Tehtävän kuvaus: </h3>
        <p>
          Tehtävän kuvaus kertoo tehtävien hakijalle, mitä tehtävä pitää sisällään. Kuvaathan siis yksityiskohtaisemmin sen, mitä kyseisessä
          tehtävässä pääsee käytännön tasolla tekemään.
        </p>
        <h3 className={classes.InstSubTitle}>1.4 Lisää kuva raahaamalla tai valitsemalla tiedostoista: </h3>
        <p>
          Kuva on olennainen osa tehtävän nimen lisäksi jonka tehtävien hakija näkee. Valitsethan siis tehtävän
          nimeen ja tehtävän laatuun sopivan kuvan. Voit joko raahata tiedoston kenttään tai valitsemalla kuvan omista tiedostoista.
        </p>
        <h3 className={classes.InstSubTitle}>1.5 Valitse tehtävän ajankohta: </h3>
        <p>
          Tehtävien ajankohdalla tarkoitetaan sitä vuorokaudenaikaa, jolloin tehtävää on mahdollista tehdä. Esimerkiksi, onko tehtävä luonteeltaan sellainen
          että se painottuu aamuun, vai onko tehtävää mahdollista tehdä vain iltaisin. Annathan siis tiedon siitä,
          mihin vuorokaudenaikaan tehtävää voi olla tekemässä, jotta hakija tietää miten tehtävä sopii hänen aikatauluihinsa.
          HUOM! Mikäli tehtävää voi tehdä joustavasti vuorokaudenajasta riippumatta voit valita myös useamman ajankohdan!
        </p>
        <h3 className={classes.InstSubTitle}>1.6 Valitse attribuutit: </h3>
        <p>
          Sun Vuorossa käyttäjä hakee tehtäviä kolmen eri ominaisuuden arvojen perusteella. Onkin tärkeää, että tehtävälle lisätään
          sen luonnetta kuvastavat attribuuttien arvot. Arvoja on kolme, jotka ovat fyysisyys, sosiaalisuus ja pulmanratkaisu. Jokaiselle attribuutille
          annetaan arvo liukuvalintaa käyttämällä väliltä 1-5, yhden ollessa pienin ja viiden suurin.
        </p>
        <h3 className={classes.InstSubTitle}>1.7 Tehtävän tallennus: </h3>
        <p>
          Tarkista, että antamasi tiedot ovat oikein ja että kaikki kentät ovat täytetty. Voit sen jälkeen
          tallentaa tehtävän painamalla vihreää &quot;Lisää tehtävä&quot; painiketta lomakkeen lopussa.
        </p>
        <div className={classes.InstHrLine} />
        <div className={classes.ContentsLink}>
        <a href="#top"> ᐃ Takaisin sisällysluetteloon ᐃ </a>
        </div>
      </div>
    </div>
    <div style={{ paddingTop: '1rem', border: '1rem solid #ffe79e' }}>
      <h2 className={classes.InstTitle} id="mod" > 2. Olemassa olevan tehtävän muokkaus </h2>
      <div className={classes.Text}>
        <p>
          Valitse &quot;Muokkaa tehtävää&quot; keltaisella pohjalla klikkaamalla sitä. Alle Aukeaa nykyiset tehtävät. Klikkaa sitä tehtävää,
          jota haluat muokattavan. Uusi ikkuna aukeaa ja ikkunassa näät kyseisen tehtävän nykyiset tiedot.
          Mikäli haluat muokata vain tiettyä ominaisuutta, ei sinun tarvitse huolehtia muista tiedoista, vaan ne pysyvät
          automaattisesti samana kuin aiemmin. Muokkaa siis vain niitä tietoja, joita tarvitsee muuttaa.
          Tehtävän tiedot ovat seuraavat (järjestyksessä ylhäältä alas):
        </p>
        <p>Muutosten tallennus tehään lomakkeen lopusta löytyvällä painikkeella!</p>
        <h3 className={classes.InstSubTitle}>2.1 Muokkaa tehtävän nimi: </h3>
        <p>
          Tekstikentässä näkyy valmiina tehtävän nykyinen nimi. Voit antaa uuden nimen poistamalla tekstikentästä vanhan nimeen
          ja kirjoittamalla tyhjään tekstikenttään uuden nimen.
        </p>
        <h3 className={classes.InstSubTitle}>2.2 Muokkaa tehtävän paikka: </h3>
        <p>
          Nimi kentän perästä löydät paikan, joka on automaattisesti oma organisaatiosi, eikä se siis ole muutettavissa. Mikäli kuitenkin huomaat,
          että organisaatio on väärä, niin otathan yhteyden sivun ylläpitäjään.
        </p>
        <h3 className={classes.InstSubTitle}>2.3 Muokkaa tehtävän kuvaus: </h3>
        <p>
          Tehtävän nimen alta löydät nykyisen tehtävän kuvauksen. Voit joko poistaa koko kuvauksen ja kirjoittaa tyhjään kenttään uuden
          tai muokata vanhaa kuvausta niiltä osin, kuin on tarpeellista.
        </p>
        <h3 className={classes.InstSubTitle}>2.4 Muokkaa kuva raahaamalla tai valitsemalla tiedostoista: </h3>
        <p>
          Nykyisen kuvan näät kuvanvalintakentän alapuolella. Uuden kuvan voit joko raahata merkattuun kenttään tai
          valitsemalla kuvan omista tiedostoista klikkaamalla &quot;Choose file&quot; painiketta.
        </p>
        <h3 className={classes.InstSubTitle}>2.5 Muokkaa tehtävän vuorokaudenaikoja: </h3>
        <p>
          Vuorokaudenajoilla tarkoitetaan sitä vuorokaudenaikaa, jolloin tehtävää on mahdollista tehdä. Esimerkiksi, onko tehtävä luonteeltaan sellainen
          että se painottuu aamuun, vai onko tehtävää mahdollista tehdä vain iltaisin. Annathan siis tiedon siitä,
          mihin vuorokaudenaikaan tehtävää voi olla tekemässä, jotta hakija tietää miten tehtävä sopii hänen aikatauluihinsa.
          HUOM! Mikäli tehtävää voi tehdä joustavasti vuorokaudenajasta riippumatta voit valita myös useamman ajankohdan!
        </p>
        <h3 className={classes.InstSubTitle}>2.6 Muokkaa attribuutit: </h3>
        <p>
          Mikäli haluat muokata tehtävän vaatimia attribuutteja, voit antaa jokaiselle attribuutille
          arvon liukuvalintaa käyttämällä väliltä 1-5, yhden ollessa pienin ja viiden suurin. Sinun tarvitsee muuttaa vain sitä arvoa, jota tarvitsee!
          Muut pysyvät automaattisesti samana kuin aiemmin.
        </p>
        <h3 className={classes.InstSubTitle}>2.7 Muutosten tallennus: </h3>
        <p>
          Tarkista, että antamasi tiedot ovat oikein. Voit sen jälkeen
          tallentaa tehtävään tehdyt muutokset painamalla keltaista &quot;Tallenna muutokset&quot; painiketta lomakkeen lopussa.
        </p>
        <div className={classes.InstHrLine} />
        <div className={classes.ContentsLink}>
        <a href="#top"> ᐃ Takaisin sisällysluetteloon ᐃ </a>
        </div>
      </div>
    </div>

    <div style={{ paddingTop: '1rem', border: '1rem solid #ff7e7e' }}>
      <h2 className={classes.InstTitle} id="del" > 3. Olemassa olevan tehtävän poisto </h2>
      <div className={classes.Text}>
        <p>
          Valitse &quot;Poista tehtävä&quot; punaisella pohjalla klikkaamalla sitä. Alle Aukeaa nykyiset tehtävät. Klikkaa sitä tehtävää, jonka haluat poistaa.
          Uusi ikkuna aukeaa ja kyseisessä ikkunassa näät kyseisen tehtävän tiedot. Kun olet varma, että haluat poistaa tehtävän,
          selaa tehtäväkuvauksen alaosaan, josta löydät punaisen &quot;Poista tehtävä&quot; painikkeen. Painikkeesta painamalla tehtävä poistetaan.
        </p>
        <p className={classes.InstSubTitle}>HUOM! Tehtävän poisto on peruuttamaton toiminto, eikä tehtävän tietoja saa enää palautettua! </p>
        <div className={classes.InstHrLine} />
        <div className={classes.ContentsLink}>
        <a href="#top"> ᐃ Takaisin sisällysluetteloon ᐃ </a>
        </div>
      </div>
    </div>
  </div>
);

export default admininstructions;
