import React from 'react';

const organizationList = (props) => {

    console.log(props);
    const orgs = props.orgs.map(org => {
        return <p>{org.organization_name}</p>
    })

    return(
        <div>{orgs}</div>
    );

}

export default organizationList;