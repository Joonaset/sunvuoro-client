import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import TaskModal from './TaskModal';

it('TaskModal should not have basic accessibility issues', async () => {
  const { container } = render(<TaskModal
    name="name"
    modalState={false}
    city="city"
    description="description"
    imageAlt="alt text"
    link="link"
    organizationName="organization name"
    address="addrsss"
    postcode="postcode"
    phone="phone"
    email="email"
    workTimes="worktimes"
    taskWhen="taskWhen"
    longitude="longitude"
    latitude="latitude"
    handleValueChange={() => true}
  />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
