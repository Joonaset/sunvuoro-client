import React from 'react';
import { Modal, Image, Tabs, Tab, Form, Button } from 'react-bootstrap';
import classes from './Task.module.css';
import LocationMap from '../Map';
import newImage from '../../images/add_new_taskmodal.jpg';

const taskModal = ({
  name,
  modalState,
  closeModal,
  city,
  description,
  image,
  imageAlt,
  link,
  organization_name: organizationName,
  address,
  postcode,
  phone,
  email,
  titleRef,
  workTimes,
  task_when: taskWhen,
  longitude,
  latitude,
  handleValueChange,
  sendEmail,
  closeSuggestionModal,
  fysiikka_value,
  sosiaalisuus_value,
  ajattelu_value
}) => {
  const modalWithData = (
    // start links with https:// to ensure correct navigating
    link=link.substring(0,8)=="https://" ? link : "https://"+link,
      <Modal
        show={modalState}
        onHide={closeModal}
        dialogClassName="modal-width"
        aria-modal="true"
        aria-label="Tehtäväikkuna"
      >
        <Modal.Header closeButton>
          <Modal.Title>
            <h1 tabIndex="-1" ref={titleRef}>{`${name}, ${city}`}</h1>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs defaultActiveKey="info" id="uncontrolled-tab-example">
            <Tab eventKey="info" title="Tehtävän kuvaus">
              <p className={classes.SingleDescription}>{description}</p>
              <div className={classes.ImageContactContainer}>
                <div className={classes.InfoContainer}>
                  <div className={classes.TaskInformation}>
                    <table className={classes.ValueTable}>
                      <thead>
                        <tr style={{ backgroundColor: '#fff' }}>
                          <th colSpan="2"><h2>Tehtävän tiedot</h2></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style={{ backgroundColor: '#fff' }}>
                          <td colSpan="2" style={{ fontStyle: 'italic', fontSize: '0.8em' }}>
                            <span> Fysiikka, sosiaalisuus ja pulmanratkaisu ilmaistaan arvoina yhdestä viiteen siten että 1 on pienin ja 5 on suurin</span>
                          </td>
                        </tr>
                        <tr style={{ backgroundColor: '#f2f5f8' }}>
                          <td>Fysiikka</td>
                          <td>{fysiikka_value}</td>
                        </tr>
                        <tr style={{ backgroundColor: '#f2f5f8' }}>
                          <td>Sosiaalisuus</td>
                          <td>{sosiaalisuus_value}</td>
                        </tr>
                        <tr style={{ backgroundColor: '#f2f5f8' }}>
                          <td>Pulmanratkaisu</td>
                          <td>{ajattelu_value}</td>
                        </tr>
                        <tr style={{ backgroundColor: '#f2f5f8' }}>
                          <td>Ajankohta</td>
                          <td>{workTimes}</td>
                        </tr>
                      </tbody>
                    </table>
                    {/* <p style={{fontWeight:'bold'}}>Tehtävän tiedot</p>
                    <p>Fysiikka: {fysiikka_value} <span style={{fontStyle:'italic', fontSize:'0.8em'}}>(1: pienin, 5: suurin)</span></p>
                    <p>Sosiaalisuus: {sosiaalisuus_value} <span style={{fontStyle:'italic', fontSize:'0.8em'}}>(1: pienin, 5: suurin)</span></p>
                    <p>Pulmanratkaisu: {ajattelu_value} <span style={{fontStyle:'italic', fontSize:'0.8em'}}>(1: pienin, 5: suurin)</span></p>
                    <p>Ajankohta: {taskWhen || 'Keskusteltavissa'}</p> */}
                  </div>
                  <div className={classes.SingleContactInfo}>
                    <h2 style={{ fontWeight: 'bold', margin: '1rem', fontSize: '1.4rem', textDecoration: 'underline' }}>
                      <a href={link} target="_blank" >{organizationName}</a>
                    </h2>
                    <h3 style={{ fontWeight: 'bold', marginBottom: '0', float: 'left' }}>Osoite</h3>
                    {`${address},`}
                    <br />
                    {`${postcode} ${city}`}
                    <br />
                    <h3 style={{ fontWeight: 'bold', marginTop: '1rem', marginBottom: '0', float: 'left' }}>Ota yhteyttä</h3>
                    {phone}
                    <br />
                    {email}
                    <br />
                  </div>
                  <Image src={image} className={classes.SingleImage} alt={imageAlt} />
                </div>
              </div>
            </Tab>
            <Tab eventKey="map" title="Sijainti kartalla" aria-describedby="location-desc">
              <div aria-hidden="true" tabIndex="-1" className={classes.MapContainer}>
                <LocationMap longitude={longitude} latitude={latitude} />
              </div>
              <div id="location-desc" style={{ padding: '1rem 0 0 1rem' }}>{`Tehtävä sijaitsee osoitteessa ${address}, ${postcode} ${city}`}</div>
            </Tab>
          </Tabs>
        </Modal.Body>
        <div className={classes.FormContainer}>
          <h2>
            Yhteydenottolomake
          </h2>
          <p>
            {`Jos tehtävä herätti mielenkiintosi, jätä yhteystietosi alle, niin sinuun otetaan yhteyttä!
             Tämän lomakkeen tiedot lähetetään sähköpostilla organisaatiolle ${organizationName}. Tietoja ei säilytetä järjestelmässämme.`}
          </p>
          <Form>
            <Form.Group controlId="formBasicName">
              <Form.Label className={classes.ContactMessageLabel}>Nimesi</Form.Label>
              <Form.Control onChange={handleValueChange('applicationSender')} style={{ marginTop: '1vh' }} type="textarea" placeholder="Nimesi" />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label className={classes.ContactMessageLabel}>Sähköpostiosoitteesi tai puhelinnumerosi</Form.Label>
              <Form.Control
                onChange={handleValueChange('applicationEmail')}
                style={{ marginTop: '1vh' }}
                type="email"
                placeholder="Sähköpostiosoitteesi tai puhelinnumerosi"
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Label className={classes.ContactMessageLabel}>Viestisi</Form.Label>
              <Form.Control
                readOnly
                defaultValue={`Hei, \nolen kiinnostunut tehtävästänne ${name}.`}
                onChange={handleValueChange('applicationBody')}
                style={{ marginTop: '1vh' }}
                as="textarea"
                rows="3"
              />
            </Form.Group>
            <Button variant="success" className="save-btn" onClick={() => sendEmail()}>Lähetä viesti</Button>
          </Form>
        </div>
      </Modal>
  );
  const modalWithNoData = (
    <Modal
      show={modalState}
      dialogClassName="modal-width"
      onHide={closeModal}
      aria-modal="true"
      aria-labelledby="modal-title"
      aria-describedby="description-text"
    >
      <Modal.Header closeButton>
        <Modal.Title id="modal-title">
          Etkö löytänyt sopivaa tehtävää?
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Tabs defaultActiveKey="info" id="uncontrolled-tab-example">
          <Tab eventKey="info" title="Ehdota uutta tehtävää" width="60">
            <p id="description-text" className={classes.SingleDescription}>
              Etkö löytänyt tällä kertaa sinulle sopivaa tehtävää? Täytä alle toivomasi tehtävän tiedot, ja teemme parhaamme vastataksemme toiveisiisi!
            </p>
            <div className={classes.ImageContactContainer}>
              <Image src={newImage} className={classes.SingleImage} />
            </div>
          </Tab>
        </Tabs>
      </Modal.Body>
      <div className={classes.FormContainer}>
        <h1 className={classes.Formtitle}>
          <br />
          Täytä alle toivomasi tehtävän tiedot.
        </h1>
        <Form>
          <Form.Group controlId="formBasicTaskName">
            <Form.Control onChange={handleValueChange('applicationSender')} style={{ marginTop: '1vh' }} type="textarea" placeholder="Tehtävän nimi" />
          </Form.Group>
          <Form.Group controlId="formBasicTaskDescription">
            <Form.Control onChange={handleValueChange('applicationSender')} style={{ marginTop: '1vh' }} type="textarea" placeholder="Tehtävän kuvaus" />
          </Form.Group>
          <Form.Group controlId="formBasicTaskOrganization">
            <Form.Control
              onChange={handleValueChange('applicationSender')}
              style={{ marginTop: '1vh' }}
              type="textarea"
              placeholder="Organisaatio (esim. Annalan huvila)"
            />
          </Form.Group>
          <Form.Group controlId="formBasicTaskWhen">
            <Form.Control onChange={handleValueChange('applicationSender')} style={{ marginTop: '1vh' }} type="textarea" placeholder="Tehtävän ajankohta" />
          </Form.Group>
          <Button variant="success" onClick={() => closeSuggestionModal()}>Lähetä ehdotus</Button>
        </Form>
      </div>
    </Modal>
  );

  return name ? modalWithData : modalWithNoData;
};

export default taskModal;
