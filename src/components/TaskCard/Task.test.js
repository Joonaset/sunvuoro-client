import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Task from './Task';

it('Task should not have basic accessibility issues', async () => {
  const { container } = render(<Task name="task name" city="city name" />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});
