import React from 'react';
import { Card, Col } from 'react-bootstrap';
import Image from '../Image/Image';
import classes from './Task.module.css';

const Task = ({ name, onClick, image, city }) => {
  const styleAddNew = name.toLowerCase().includes('ehdota') ? 'linear-gradient(rgb(102, 222, 217) 0%, rgb(255, 255, 255) 50%)' : '';
  return (
    <Col md={4} s={1}>
      <button type="button" className={classes.Card} onClick={onClick} style={{ background: styleAddNew }}>
        <Image image={image} imageAlt="" />
        <Card.Body>
          <Card.Title role="heading" aria-level="5">{name}</Card.Title>
          <Card.Subtitle role="heading" aria-level="6">{city}</Card.Subtitle>
        </Card.Body>
      </button>
    </Col>
  );
};

export default Task;
