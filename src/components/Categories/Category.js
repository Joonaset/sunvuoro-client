import React from 'react';
import { Col } from 'react-bootstrap';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const category = ({ label, desc, catValue, handleChange }) => (
  <Col m={4} s={1} aria-labelledby={`${label}-slider-label`} aria-describedby={`${label}-slider-desc`}>
    <h2 id={`${label}-slider-label`}>{label}</h2>
    <p id={`${label}-slider-desc`}>{desc}</p>
    <Slider
      value={catValue}
      min={1}
      max={5}
      dots
      onChange={handleChange}
      marks={{
        1: 1, 2: 2, 3: 3, 4: 4, 5: 5
      }}
      trackStyle={{ background: 'linear-gradient(to right, rgba(91, 37, 109, 0.75) 0%, rgb(91, 37, 109) 100%)' }}
      railStyle={{ backgroundColor: '#00000' }}
      handleStyle={{ borderColor: 'rgb(91, 37, 109)' }}
      activeDotStyle={{ borderColor: 'rgb(91, 37, 109)' }}
    />
  </Col>
);

export default category;
