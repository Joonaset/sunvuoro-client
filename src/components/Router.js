import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from './Main/Main';
import TasksPage from '../containers/TasksPage';
import QuizPage from '../containers/QuizPage/QuizPage';
import ProfessionalPage from '../containers/ProfessionalPage/ProfessionalPage';
import Footer from './Footer/Footer';
import AdminPage from '../containers/AdminPage/AdminPage';
import RegisterPage from '../containers/RegisterPage/RegisterPage'
import StatsPage from '../containers/StatisticsPage/StatisticsPage';
import PrivateRoute from '../utils/PrivateRoute';
import { getNavigationBar } from '../utils/DivUtils';
import OrganizationPage from '../containers/OrganizationPage/OrganizationPage';
import ApplicationsPage from '../containers/ApplicationsPage/ApplicationsPage';
import Saavutettavuusseloste from '../containers/Saavutettavuusseloste/Saavutettavuusseloste';
import FeedbackForm from '../containers/Saavutettavuusseloste/FeedbackForm';

const Router = () => {
  const [updateAdminNav, setUpdateAdminNav] = useState(false);

  return (
    <BrowserRouter>
      <header role="banner">
        <a className="skip-to-content" href="#main-content">Siirry sisältöön</a>
        {getNavigationBar(updateAdminNav)}
      </header>
      <main id="main-content" role="main">
        <Switch>
          <Route path="/" exact component={Main} />
          <Route path="/tasks" exact component={TasksPage} />
          <Route path="/quiz" exact component={QuizPage} />
          <Route path="/login" exact component={() => <ProfessionalPage setUpdateAdminNav={setUpdateAdminNav} />} />
          <Route path="/register" exact component={RegisterPage} />
          <Route path="/saavutettavuusseloste" exact component={Saavutettavuusseloste} />
          <Route path="/feedback" exact component={FeedbackForm} />
          <PrivateRoute path="/admin" component={AdminPage} user="admin"/>
          <PrivateRoute path="/organizations" component={OrganizationPage} user="admin"/>
          <PrivateRoute path="/applications" component={ApplicationsPage} user="maintainer"/>
          <PrivateRoute path="/stats" component={StatsPage} user="admin" />
        </Switch>
      </main>
      <footer role="contentinfo">
        <Footer />
      </footer>
    </BrowserRouter>
  );
};
export default Router;
