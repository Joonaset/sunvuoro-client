import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { LinkContainer } from 'react-router-bootstrap';
import { NavDropdown } from 'react-bootstrap';
import classes from './Navigation.module.css';
import { removeUserSession, getToken, getUser } from '../../utils/AuthUtils';
import { changeDivDisplayStyle } from '../../utils/DivUtils';

const navigation = ({ style }) => (
  <Navbar role="navigation" aria-label="Toggle navigation" collapseOnSelect expand="md" bg="sun-vuoro" variant="dark" className="Navbar">
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav" style={{ width: '100%' }}>
      <Nav className="mr-auto" style={{ width: '100%' }}>
        <LinkContainer to="/" exact>
          <Nav.Link>Sun vuoro</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/quiz" exact>
          <Nav.Link>Löydä tehtäväsi</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/tasks" exact>
          <Nav.Link>Kaikki tehtävät</Nav.Link>
        </LinkContainer>
        <div className={classes.AdminpaneContainer}>
          <LinkContainer to="/login" exact>
            <Nav.Link>Tehtävien tarjoajalle</Nav.Link>
          </LinkContainer>
          <div id="adminpane" style={style} className={classes.AdminpaneMobile}>
            <NavDropdown menuRole="menu" title="Hallintapaneeli" id="basic-nav-dropdown">
              <NavDropdown.Item role="menuitem" href="/admin">
                Tehtävien hallinta
              </NavDropdown.Item>
              <NavDropdown.Item role="menuitem" href="/organizations">
                Organisaatioiden hallinta
              </NavDropdown.Item>
              {getUser().isAdmin
                ? <NavDropdown.Item role="menuitem" href="/applications">
                  Hakemusten hallinta
                </NavDropdown.Item>
                : <div />}
              <NavDropdown.Item role="menuitem" href="/stats">
                Tilastot
              </NavDropdown.Item>
            </NavDropdown>
            <LinkContainer
              className={classes.SignOut}
              to="/"
              exact
              onClick={() => { removeUserSession(); changeDivDisplayStyle('adminpane', false); }}
            >
              <Nav.Link className={classes.SignOut}>Kirjaudu ulos</Nav.Link>
            </LinkContainer>
          </div>
        </div>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default navigation;
