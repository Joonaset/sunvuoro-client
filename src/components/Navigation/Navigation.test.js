import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Navigation from './Navigation';

it('Navigation should not have basic accessibility issues', async () => {
  const { container } = render(<Router><Navigation /></Router>);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});