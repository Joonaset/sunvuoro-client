import React, { useState, useEffect } from 'react';
import { Nav } from 'react-bootstrap';

const AdminNav = (props) => {

    const [activeKey, setActiveKey] = useState({
        activeKey: ''
    })

    return(
        <Nav variant="pills" activeKey={activeKey} onSelect={setActiveKey}>
            <Nav.Item>
                <Nav.Link eventKey="link-0" href="/admin">Hallintapaneeli</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link eventKey="link-1" href="/admin/tasks">Tehtävien hallinta</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link eventKey="link-2" href="/admin/organizations">Organisaatioiden hallinta</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link eventKey="link-3" href="/admin/applications">Hakemusten hallinta</Nav.Link>
            </Nav.Item>
        </Nav>
    );
}

export default AdminNav;
