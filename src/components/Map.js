import React from 'react';
import Map from 'pigeon-maps';
import Marker from 'pigeon-marker';
import Overlay from 'pigeon-overlay';

import classes from './Map.module.css';
import error from '../images/error.jpg';

const MAPTILER_ACCESS_TOKEN = 'mnTO74IBkVrqR2xs3Tal';
const MAP_ID = 'streets';

function mapTilerProvider(x, y, z, dpr) {
  return `https://api.maptiler.com/maps/${MAP_ID}/256/${z}/${x}/${y}${dpr >= 2 ? '@2x' : ''}.png?key=${MAPTILER_ACCESS_TOKEN}`;
}

const LocationMap = ({ longitude, latitude }) => (
  longitude && latitude
    ? <Map
      className={classes.MapContainer}
      provider={mapTilerProvider}
      defaultCenter={[latitude, longitude]}
      defaultZoom={15}
      defaultWidth={766}
      defaultHeight={406}
    >
      <Marker
        alt="Testi"
        anchor={[latitude, longitude]}
        payload={1}
        onClick={() => window.open(`https://www.google.com/maps/dir/?api=1&destination=${latitude},${longitude}`, 'Google maps')}
      />
      <Overlay anchor={[latitude, longitude]} offset={[120, 79]} />
    </Map>
    : <img alt="Karttaa ei voitu ladata" src={error}  style={{maxWidth:'100%', maxHeight:'100%'}}/>
);

export default LocationMap;
