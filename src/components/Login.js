import React from 'react';
import { Button, Col } from 'react-bootstrap';
import classes from '../containers/ProfessionalPage/ProfessionalPage.module.css';

const Login = (props) => (
  <div style={{ backgroundColor: '#fff', padding: '2rem' }}>
    <h1 style={{ textAlign: 'center', marginBottom: '0' }}>Kirjaudu sisään</h1>
    <form method="POST">
      <div className="form-group" style={{ marginTop: '1vh' }}>
        <label htmlFor="username-field">Käyttäjätunnus</label>
        <input
          id="username-field"
          type="text"
          name="username"
          className="form-control"
          value={props.username}
          onChange={props.usernameChange()}
          onKeyPress={(event) => {
            if (event.keyCode === 13) {
              props.login();
            }
          }}
          placeholder="Käyttäjätunnus"
        />
      </div>
      <div className="form-group">
        <label htmlFor="password-field">Salasana</label>
        <input
          id="password-field"
          type="password"
          name="password"
          className="form-control"
          value={props.password}
          onChange={props.passwordChange()}
          placeholder="Salasana"
        />
      </div>
      <Col className={classes.ButtonContainer}><Button style={{ paddingLeft: '1.6rem', paddingRight: '1.6rem' }} className="sun-vuoro-btn" onClick={props.login()}>Kirjaudu</Button></Col>
    </form>
    <p style={{ marginTop: '2rem' }}>Tai rekisteröidy uudeksi tilojen tarjoajaksi täyttämällä hakemus:</p>
    <Col className={classes.ButtonContainer}>
      <Button
        variant="success"
        className="sun-vuoro-btn"
        href="/register"
      >
        Rekisteröidy
      </Button>
    </Col>
  </div>
);

export default Login;
