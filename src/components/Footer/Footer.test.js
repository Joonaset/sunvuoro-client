import React from 'react';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import Footer from './Footer';

it('Footer should not have basic accessibility issues', async () => {
  const { container } = render(<Footer />);
  const results = await axe(container);
  expect(results).toHaveNoViolations();
});