import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { hanldeFontSizeChange } from '../../utils/util';
import classes from './Footer.module.css';
import HVTLogo from '../../images/HVT_Logo.png';
import HYLogo from '../../images/HY_Logo.png';
import LJLogo from '../../images/LJ_Logo.png';
import MPLogo from '../../images/MP_Logo.png';
import SLogo from '../../images/S_Logo.png';
import SJKLogo from '../../images/SJK_Logo.png';

const Footer = () => {
  return (
    <Row className={classes.FooterContainer}>
      <Col aria-label="Vaihda tekstin kokoa" className={classes.FontSizeContainer}>
        <button aria-label="Normaali tekstikoko" type="button" onClick={() => hanldeFontSizeChange('normal')} className={classes.FontSizeNormal}>A</button>
        <button aria-label="Suuri tekstikoko" type="button" onClick={() => hanldeFontSizeChange('medium')} className={classes.FontSizeMedium}>A</button>
        <button aria-label="Suurin tekstikoko" type="button" onClick={() => hanldeFontSizeChange('large')} className={classes.FontSizeLarge}>A</button>
      </Col>
      <Col className={classes.ContactInfo}>
        <p>Ota yhteyttä</p>
        <p>puh. 012 345 5678</p>
        <p>esimerkki@esimerkki.com</p>
        <p className={classes.SaavutettavuusLinkContainer}>
          <a href="/saavutettavuusseloste" exact="true">Saavutettavuusseloste</a>
        </p>
      </Col>
      <Col m={12} className={classes.FooterCol}>
        <img className={classes.FooterImage} alt="Hyvinvoinnin tilat" src={HVTLogo} />
        <img className={classes.FooterImage} alt="Metropolia ammattikorkeakoulu" src={MPLogo} />
        <img className={classes.FooterImage} alt="LAB University of Applied Sciences" src={SLogo} />
        <img className={classes.FooterImage} alt="Seinäjoen kansalaisopisto" src={SJKLogo} />
        <img className={classes.FooterImage} alt="Hyötykasviyhdistys" src={HYLogo} />
        <img className={classes.FooterImage} alt="Lapinjärvi" src={LJLogo} />
      </Col>
    </Row>
  );
};

export default Footer;
